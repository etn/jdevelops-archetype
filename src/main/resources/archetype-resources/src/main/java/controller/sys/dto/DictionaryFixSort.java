package ${package}.controller.sys.dto;

import cn.tannn.jdevelops.annotations.jpa.JpaUpdate;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 修改字典顺序
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Schema(description = "修改字典顺序")
@ToString
@Getter
@Setter
public class DictionaryFixSort {

    @Schema(description = "id", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    @JpaUpdate(unique = true)
    private Integer id;

    /**
     * 名称;同级唯一
     */
    @Schema(description = "排序;越小排序越靠前，默认999", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    private  Integer sort ;

}
