package ${package}.controller.user.dto;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

/**
 * 编辑用户
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/11/21 9:56
 */
@Schema(description = "编辑用户")
@ToString
@Getter
@Setter
public class CustomerEdit extends SerializableBean<CustomerEdit> {
    /**
     * 用户id
     */
    @Schema(description = "用户id",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    Long id;


    /** 用户昵称 */
    @Schema(description = "用户昵称")
    private  String  nickname ;


    /** 用户姓名 */
    @Schema(description = "用户姓名")
    private  String  name ;


    /** 性别;0[未知]，1[男性]，2[女性] */
    @Schema(description = "性别(0[未知]，1[男性]，2[女性])")
    @Min(0)
    @Max(2)
    private  Integer gender ;



}
