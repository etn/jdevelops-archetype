package ${package}.logs.entity;

import ${package}.customer.constant.LoginType;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import cn.tannn.jdevelops.annotations.web.constant.PlatformConstant;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;

/**
 * 登录日志  todo 大项目可以放到es里
 *  <p> ip + status + des 可以观测恶意登录
 *  <p> ip + status + des + name为空， loginname不空 可以观测恶意登录且为字典攻击
 *
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 10:31
 */
@Entity
@Table(name = "log_user_login")
@Comment("登录日志")
@Getter
@Setter
@ToString
@DynamicUpdate
@DynamicInsert
public class LoginLog extends SerializableBean<LoginLog> {
    /**
     * 时间戳的UUID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuidCustomGenerator")
    @GenericGenerator(name = "uuidCustomGenerator", strategy = "cn.tannn.jdevelops.jpa.generator.UuidCustomGenerator")
    @Column(columnDefinition="bigint")
    @Comment("时间戳的UUID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;


    /**
     * 访问设备信息
     */
    @Comment("访问设备信息")
    @Column(columnDefinition = "varchar(100)")
    private String userAgent;

    /**
     * 登录的IP
     */
    @Comment("登录的IP")
    @Column(columnDefinition = "varchar(50)")
    private String  ipAddress;

    /**
     * 设备登录信息
     */
    @Comment("ip归属地[国家-区域-省份-城市-ISP]")
    @Column(columnDefinition = "varchar(100)")
    private String ipRegion;

    /**
     * 登录时间（yyyy-MM-dd HH:mm:ss）
     */
    @Comment("登录时间（yyyy-MM-dd HH:mm:ss）")
    @Column(columnDefinition = "varchar(50)")
    private String loginTime;

    /**
     * 登录者的中文名
     */
    @Comment("登录者的中文名")
    @Column(columnDefinition = "varchar(100)")
    private String userName;

    /**
     * 登录者的登录名
     */
    @Comment("登录者的登录名")
    @Column(columnDefinition = "varchar(100)")
    private String loginName;

    /**
     * 登录者的登录名
     */
    @Comment("登录者Id")
    @Column(columnDefinition = "bigint")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 登录类型
     * @see LoginType
     */
    @Comment("登录类型")
    @Column(columnDefinition = "varchar(100)")
    private String type;


    /**
     * 登录状态：0[失败],1[成功]
     */
    @Column(columnDefinition = " int default 1")
    @Comment("登录状态:0[失败],1[成功]")
    private Integer status;


    /**
     * 登录的Token
     */
    @Column(columnDefinition = "text")
    @Comment("登录的Token")
    private String token;

    /**
     * token 来源 [逗号隔开，一般不会有多个]
     * @see PlatformConstant
     */
    @Column(columnDefinition = "varchar(100)")
    @Comment("token来源[逗号隔开，一般不会有多个]")
    private String platform;

    /**
     * 备注
     */
    @Comment("备注")
    @Column(columnDefinition = "varchar(100)")
    private String description;
}
