package ${package}.customer.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 菜单状态
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 13:21
 */
@AllArgsConstructor
@Getter
public enum MenuStatus {


    /**
     * 禁用
     */
    DISABLE(0, "禁用"),


    /**
     * 正常
     */
    NORMAL(1, "启用"),


    ;

    private final int status;
    private final String description;


    /**
     * 获取枚举实例
     *
     * @param status int
     * @return MenuStatus
     */
    public static MenuStatus fromValue(Integer status) {
        for (MenuStatus roleStatus : MenuStatus.values()) {
            if (Objects.equals(status, roleStatus.getStatus())) {
                return roleStatus;
            }
        }
        return null;
//        throw new IllegalArgumentException();
    }
}
