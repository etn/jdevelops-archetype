package ${package};

import cn.tannn.jdevelops.autoschema.scan.EnableAutoSchema;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 模板启动类
 * @author tn
 */
@SpringBootApplication
@EnableAsync
@EnableAutoSchema
public class ${application} {

    public static void main(String[] args) {
        SpringApplication.run(${application}.class, args);
    }

}
