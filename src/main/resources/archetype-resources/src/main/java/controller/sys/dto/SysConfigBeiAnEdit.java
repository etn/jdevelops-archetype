package ${package}.controller.sys.dto;

import ${package}.sys.domain.SysBeiAn;
import ${package}.sys.domain.SysConfigEnum;
import ${package}.sys.entity.SysConfig;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 系统配置表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Schema(description = "编辑系统基础信息")
@ToString
@Getter
@Setter
public class SysConfigBeiAnEdit {

    /**
     * 备案信息
     */
    @Schema(description = "备案信息")
    private String description;



    public SysConfig toSysConfig(){
        SysConfig config = new SysConfig();
        config.setCode(SysConfigEnum.BEI_AN.getCode());
        config.setTitle(SysConfigEnum.BEI_AN.getTitle());
        SysBeiAn beiAn = new SysBeiAn();
        // ps，如果不修改请保持原值传回来
        beiAn.setDescription(description);
        config.setData(beiAn.toJson());
        return config;
    }

}
