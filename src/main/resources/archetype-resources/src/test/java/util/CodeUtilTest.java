package ${package};

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CodeUtilTest {

    @Test
    void dictCodeLevel() {
        assertThrows(IllegalArgumentException.class, () -> CodeUtil.dictCodeLevel(null));
        assertThrows(IllegalArgumentException.class, () -> CodeUtil.dictCodeLevel("0"));
        assertThrows(IllegalArgumentException.class, () -> CodeUtil.dictCodeLevel("11"));
        assertEquals(1,CodeUtil.dictCodeLevel("100"));
        assertEquals(1,CodeUtil.dictCodeLevel("200"));
        assertEquals(2,CodeUtil.dictCodeLevel("100001"));
        assertEquals(3,CodeUtil.dictCodeLevel("100001001"));
    }
}
