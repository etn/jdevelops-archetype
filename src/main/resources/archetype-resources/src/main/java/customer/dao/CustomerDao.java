package ${package}.customer.dao;


import ${package}.customer.entity.Customer;
import ${package}.controller.user.dto.CustomerSettingStatus;
import cn.tannn.jdevelops.jpa.repository.JpaBasicsRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
/**
 * 系统用户(管理者)
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-10-25
 */
public interface CustomerDao extends JpaBasicsRepository<Customer,Long> {

    /**
     * 查询用户
     *
     * @param loginName 根据登录名
     * @return SysUserEntity
     */
    Optional<Customer> findByLoginName(String loginName);


    /**
     * 查询系统用户
     * @param id id
     * @return SysUser
     */
    List<Customer> findByIdIn(List<Long> id);


    /**
     * 设置用户状态
     *
     * @param status 状态:1[正常],2[锁定],3[逻辑删除]
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    @Query("update Customer u set u.status = :#{#status.status} where  u.id in (:#{#status.id}) ")
    void updateStatus(@Param("status") CustomerSettingStatus status);

}
