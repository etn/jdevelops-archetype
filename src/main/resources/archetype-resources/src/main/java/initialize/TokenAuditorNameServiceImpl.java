package ${package}.initialize;

import cn.tannn.jdevelops.jpa.auditor.AuditorNameService;
import cn.tannn.jdevelops.jwt.redis.util.RsJwtWebUtil;
import cn.tannn.jdevelops.utils.jwt.module.SignEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
/**
 * @author tan
 */
@Component
@Slf4j
public class TokenAuditorNameServiceImpl implements AuditorNameService {

    @Resource
    private HttpServletRequest request;

    @Override
    public Optional<String> settingAuditorName() {
        // 自己重新构建
        try {
            SignEntity signEntity = RsJwtWebUtil.getTokenBySignEntity(request);
            return Optional.of(signEntity.getSubject());
        } catch (Exception e) {
            log.error("自动填充数据创建者时获取当前登录用户的loginName失败");
        }
        return Optional.of("administrator");
    }
}
