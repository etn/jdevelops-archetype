package ${package}.controller.user.dto;


import cn.tannn.jdevelops.annotations.jpa.JpaSelectIgnoreField;
import cn.tannn.jdevelops.annotations.jpa.JpaSelectOperator;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLConnect;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLOperatorWrapper;
import cn.tannn.jdevelops.jpa.request.PagingSorteds;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;


/**
 * 用户分页查询
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/11/21 9:56
 */
@Schema(description = "用户分页查询")
@ToString
@Getter
@Setter
@Valid
public class CustomerPage  extends SerializableBean<CustomerPage> {

    /** 登录名 */
    @Schema(description = "登录名")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
    private  String  loginName ;

    /** 性别:0[未知]，1[男性]，2[女性] */
    @Schema(description = "性别:0[未知]，1[男性]，2[女性]")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
    @Min(0)
    @Max(2)
    private  Integer gender ;


    /** 姓名 */
    @Schema(description = "姓名")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
    private  String  name ;

    /** 状态:1[正常],2[锁定],3[逻辑删除] */
    @Schema(description = "状态:1[正常],2[锁定],3[逻辑删除]")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
    @Min(1)
    @Max(3)
    private  Integer status ;


    /**
     * 分页排序
     */
    @Schema(description = "分页排序")
    @JpaSelectIgnoreField
    @Valid
    private PagingSorteds page;


    public PagingSorteds getPage() {
        if(page == null){
            return new PagingSorteds().fixSort(1,"id");
        }
        return page;
    }
}
