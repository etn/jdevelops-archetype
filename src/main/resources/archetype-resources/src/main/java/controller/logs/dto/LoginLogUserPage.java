package ${package}.controller.logs.dto;


import cn.tannn.jdevelops.annotations.jpa.JpaSelectIgnoreField;
import cn.tannn.jdevelops.annotations.jpa.JpaSelectOperator;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLConnect;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLOperatorWrapper;
import cn.tannn.jdevelops.jpa.request.PagingSorteds;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * 登录日志查询
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 11:02
 */
@Schema(description = "登录日志分页查询")
@ToString
@Getter
@Setter
@Valid
public class LoginLogUserPage {

    /**
     * 登录状态：0[失败],1[成功]
     */
    @Schema(description = "登录状态:0[失败],1[成功]")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
    @Min(0)
    @Max(1)
    private Integer status;


    /**
     * 分页排序
     */
    @Schema(description = "分页排序")
    @JpaSelectIgnoreField
    @Valid
    private PagingSorteds page;


    public PagingSorteds getPage() {
        if(page == null){
            return new PagingSorteds().fixSort(1,"id");
        }
        return page;
    }
}
