package ${package}.controller.user.dto;

import cn.tannn.jdevelops.annotations.jpa.JpaSelectIgnoreField;
import cn.tannn.jdevelops.annotations.jpa.JpaSelectOperator;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLConnect;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLOperatorWrapper;
import cn.tannn.jdevelops.jpa.request.PagingSorteds;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * 角色-分页查询
 *
 * @author tn
 * @date 2021-06-10 16:03
 */
@Schema(description = "分页查询角色")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RolePage extends SerializableBean<RolePage> {

    /**
     * 名称，唯一
     */
    @Schema(description = "角色名称")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
    private String name;

    /**
     * 停启用状态(0、停用 ，1、启用，默认1）
     */
    @Schema(description = "停启用状态(0、停用 ，1、启用，默认1）")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
    @Max(1)
    @Min(0)
    private Integer status;

    /**
     * 分页排序
     */
    @Schema(description = "分页排序")
    @Valid
    @JpaSelectIgnoreField
    private PagingSorteds page;


    public PagingSorteds getPage() {
        if(page == null){
            return new PagingSorteds().fixSort(0,"sort");
        }
        return page.append(0,"sort");
    }
}
