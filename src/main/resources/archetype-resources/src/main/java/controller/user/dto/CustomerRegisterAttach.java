package ${package}.controller.user.dto;

import ${package}.customer.constant.CustomerStatus;
import cn.tannn.jdevelops.utils.jwt.constant.UserStatusMark;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 注册内置的一些附属信息
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/8/5 上午11:51
 */
@Schema(description = "注册内置的一些附属信息")
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRegisterAttach {


    /**
     * 审核状态:1[审核中],2[审核通过],3[审核不通过]
     */
    private Integer auditStatus;

    /**
     * @see CustomerStatus
     */
    private CustomerStatus status;

    /**
     * @see UserStatusMark
     */
    private String statusMark;


    /**
     * 备注
     */
    private String remark;
}
