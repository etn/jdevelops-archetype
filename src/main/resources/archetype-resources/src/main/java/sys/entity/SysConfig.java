package ${package}.sys.entity;

import com.alibaba.fastjson2.JSONObject;
import cn.tannn.jdevelops.jpa.convert.JsonObjectConverter;
import cn.tannn.jdevelops.jpa.modle.basics.JpaAuditFields;
import ${package}.sys.domain.SysBeiAn;
import ${package}.sys.domain.SysConfigEnum;
import ${package}.sys.domain.SysInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 系统设置
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/6 10:07
 */
@Entity
@Table(name = "sys_config")
@Comment("字典表")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
@DynamicInsert
public class SysConfig  extends JpaAuditFields<SysConfig> {

    /**
     * @see SysConfigEnum#getCode()
     */
    @Id
    @Column(columnDefinition = " varchar(200)  not null  " )
    @Comment("设置code[固定值:1.系统基础信息设置，2.系统备案信息设置]")
    @Schema(description = "设置code[固定值:1.系统基础信息设置，2.系统备案信息设置]")
    private Integer code;

    /**
     * @see SysConfigEnum#getTitle()
     */
    @Column(columnDefinition = " varchar(200)  not null  " )
    @Comment("设置描述")
    @Schema(description = "设置描述")
    private String title;

    /**
     * <p> code = 1  : {@link SysInfo}
     * <p> code = 2  : {@link SysBeiAn}
     */
    @Comment("数据json")
    @Column(columnDefinition = "json not null")
    @Convert(converter = JsonObjectConverter.class)
    @Schema(description = "数据json")
    private JSONObject data;


    public static SysConfig initInfo(){
        SysConfig config = new SysConfig();
        config.setCode(SysConfigEnum.SYS_INFO.getCode());
        config.setTitle(SysConfigEnum.SYS_INFO.getTitle());
        config.setData(SysInfo.initInfoJson());
        return config;
    }


    public static SysConfig initBeiAn(){
        SysConfig config = new SysConfig();
        config.setCode(SysConfigEnum.BEI_AN.getCode());
        config.setTitle(SysConfigEnum.BEI_AN.getTitle());
        config.setData(SysBeiAn.initBeiAnJson());
        return config;
    }


}
