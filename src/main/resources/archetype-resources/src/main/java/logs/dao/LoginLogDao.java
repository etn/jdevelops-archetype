package ${package}.logs.dao;

import ${package}.logs.entity.LoginLog;
import cn.tannn.jdevelops.jpa.repository.JpaBasicsRepository;

/**
 * 登录日志
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 10:52
 */
public interface LoginLogDao extends JpaBasicsRepository<LoginLog, Long> {
}
