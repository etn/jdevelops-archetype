package ${package}.common.exception;

import cn.tannn.jdevelops.exception.built.BusinessException;
import cn.tannn.jdevelops.result.exception.ExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 自定义异常
 *
 * @author tn
 * @date 2021-01-22 14:15
 */
@AllArgsConstructor
@Getter
public class IException extends BusinessException {

    public IException(int code, String message) {
        super(code, message);
    }

    public IException(String message) {
        super(message);
    }

    public IException(ExceptionCode errorCode) {
        super(errorCode.getCode(), errorCode.getMessage());
    }
}
