#!/bin/bash

PROJECT_PATH=${artifactId}-${version}.jar

# shutdown
pid=`ps -ef|grep $PROJECT_PATH | grep -v grep | awk '{print $2}'`
kill -9 $pid
echo "$pid进程终止成功"

