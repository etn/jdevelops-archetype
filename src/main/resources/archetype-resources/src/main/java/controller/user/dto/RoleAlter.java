package ${package}.controller.user.dto;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.constraints.NotBlank;

/**
 * 角色管理-编辑
 *
 * @author tn
 * @date 2021-06-10 15:50
 */

@Schema(description = "编辑角色")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleAlter extends SerializableBean<RoleAlter> {


    /**
     * 代号，唯一
     */
    @Schema(description = "角色编码", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String code;

    /**
     * 名称，唯一
     */
    @Schema(description = "角色名称")
    private String name;


    /**
     * 角色描述
     */
    @Schema(description = "角色描述")
    private String description;

}
