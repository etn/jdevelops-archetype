package ${package}.sys.domain;

import com.alibaba.fastjson2.JSONObject;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

/**
 * 系统基础信息
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/6 10:16
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "系统基础信息")
public class SysInfo {

    /**
     * 系统标题
     */
    @Schema(description = "系统标题")
    private String title;


    /**
     * 系统logo[base64]
     */
    @Schema(description = "系统logo[base64]")
    private String logo;


    /**
     * 系统favicon[base64]
     */
    @Schema(description = "系统favicon[base64]")
    private String favicon;


    /**
     * init
     */
    public static SysInfo initInfo() {
        SysInfo sysInfo = new SysInfo();
        sysInfo.setTitle("云图·区域防务态势情报");
        sysInfo.setLogo("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAABA5JREFUaEPtm9trF0cUxz/zy937a6to1EpERdtqoqIWkQo+96IoasSAIFSoTy0lFR9EvIGiPnkJWBWpEv8BpViv9YJIq6J4wbuvaqL+0iS/kZPtlo2ZzW9295e47m8PLAOzc2bOd75nzpmZZRXAmEY9LqfYpBRfA0OkLkHySmtOZjQ/39+g7qjRjbqGDH8BwxIE0gTlBTlmqOpfdbNSfJNwsF3wtOa4Gr1Ov0ygG/vx1yqAdTGw62JMASed7ZThlOGEzUDq0gkjtAeclOGU4YTNQOrSQQn9YgSsmQuVZdCZC6pt1z6joK0Ddp6Ca0/sdPxaRWb4WANMHRnNCFvtK49g4X7b1uZ2kQH/vhJqR8GLt/D3UxA2lIpmlKst57ichsnDYVgVXH4Ii5qi9R0Z8MF6mDUGTt2BlYeiGeOn3bQU5o6Dc/dh2YFoYxQM8Jl7UP9bNGP8tA8shzljYwbYO/urZsG8GvjjNuw558Aw1dlOj+tFsWLYa8yNRqgqg7ftMHGDA8tUlxjAK2bAgglw8hbsO+/AMtV91ICLbg0XTZT2y8OSiiUfu6W4r19+du9NpZQrVG8Zuzzcnzutq4/hu322K7+Pdlp11bB6Dgwqh84+uuEuUfCmHXb/6ey2okjojUdZCQwoByn7U9o74c2/IGUYCQy4JAODKvof6PvgBHBrW/ATmjVgCTiymZDHG3wmfQoNM/v2eCgune2A/efh+vPu0IVt2eDYfjCyAiysDq6AUoP77l0CU4aHca7gOnIWXnWkp15HJ7RYsp0XcEWp48J+KWXPYvh8BLzKws3n/6WiAh4PhbkJn8CQSufwbwIsUyDtxMXloqA36RWwBCV5epPdC6FulHOSWdscnDUbje3fOkfQSw/hh6O9a4iLy+MnvoAHVjjrNZ+4gC8+gDXH8rUO937X9zC92g6wjCBr+nWbeSwjYFuw0qUL2Dv7y+pg9lg4ew8OXnIGNtXZwjeNkU/Xj+kegG3c2DuYyZjTPzpRO9sOX+1wWpvq8hntvg8DWHSFZWHbK90Al5c6wSGImIxZ9KVzAXD6Lhy+7PRmqrMdJyxg6b8l2z2Q/Q9YUo9clAW9gIvjGvZOpERvuWB0r5C7AAvIoZXmPJuPhbhFaZO9kqdfZp3U1QU46Lr1dhqXPJyPGDeIqc/Wax3Gld0B+nOn9c8zaDicD5r5vevaaspGrSVYhRXZVtZPhyo5HvbRpxaJLxLxmy6AgA4rcuBQtVtst91hh4mXXgo4XnwU3pqU4cLPabx6TBmOFx+FtyZluPBzGq8eU4bjxUfhrSlKhovmJw+laFW1W3Uzujh+40FxXE3dpsdnclwohh+1chlmdn0jmLZZ16gMG9HMBwYXPlR80B5bUJzQOX658pO6/Q59vgp+2sDRJAAAAABJRU5ErkJggg==");
        sysInfo.setFavicon("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAABA5JREFUaEPtm9trF0cUxz/zy937a6to1EpERdtqoqIWkQo+96IoasSAIFSoTy0lFR9EvIGiPnkJWBWpEv8BpViv9YJIq6J4wbuvaqL+0iS/kZPtlo2ZzW9295e47m8PLAOzc2bOd75nzpmZZRXAmEY9LqfYpBRfA0OkLkHySmtOZjQ/39+g7qjRjbqGDH8BwxIE0gTlBTlmqOpfdbNSfJNwsF3wtOa4Gr1Ov0ygG/vx1yqAdTGw62JMASed7ZThlOGEzUDq0gkjtAeclOGU4YTNQOrSQQn9YgSsmQuVZdCZC6pt1z6joK0Ddp6Ca0/sdPxaRWb4WANMHRnNCFvtK49g4X7b1uZ2kQH/vhJqR8GLt/D3UxA2lIpmlKst57ichsnDYVgVXH4Ii5qi9R0Z8MF6mDUGTt2BlYeiGeOn3bQU5o6Dc/dh2YFoYxQM8Jl7UP9bNGP8tA8shzljYwbYO/urZsG8GvjjNuw558Aw1dlOj+tFsWLYa8yNRqgqg7ftMHGDA8tUlxjAK2bAgglw8hbsO+/AMtV91ICLbg0XTZT2y8OSiiUfu6W4r19+du9NpZQrVG8Zuzzcnzutq4/hu322K7+Pdlp11bB6Dgwqh84+uuEuUfCmHXb/6ey2okjojUdZCQwoByn7U9o74c2/IGUYCQy4JAODKvof6PvgBHBrW/ATmjVgCTiymZDHG3wmfQoNM/v2eCgune2A/efh+vPu0IVt2eDYfjCyAiysDq6AUoP77l0CU4aHca7gOnIWXnWkp15HJ7RYsp0XcEWp48J+KWXPYvh8BLzKws3n/6WiAh4PhbkJn8CQSufwbwIsUyDtxMXloqA36RWwBCV5epPdC6FulHOSWdscnDUbje3fOkfQSw/hh6O9a4iLy+MnvoAHVjjrNZ+4gC8+gDXH8rUO937X9zC92g6wjCBr+nWbeSwjYFuw0qUL2Dv7y+pg9lg4ew8OXnIGNtXZwjeNkU/Xj+kegG3c2DuYyZjTPzpRO9sOX+1wWpvq8hntvg8DWHSFZWHbK90Al5c6wSGImIxZ9KVzAXD6Lhy+7PRmqrMdJyxg6b8l2z2Q/Q9YUo9clAW9gIvjGvZOpERvuWB0r5C7AAvIoZXmPJuPhbhFaZO9kqdfZp3U1QU46Lr1dhqXPJyPGDeIqc/Wax3Gld0B+nOn9c8zaDicD5r5vevaaspGrSVYhRXZVtZPhyo5HvbRpxaJLxLxmy6AgA4rcuBQtVtst91hh4mXXgo4XnwU3pqU4cLPabx6TBmOFx+FtyZluPBzGq8eU4bjxUfhrSlKhovmJw+laFW1W3Uzujh+40FxXE3dpsdnclwohh+1chlmdn0jmLZZ16gMG9HMBwYXPlR80B5bUJzQOX658pO6/Q59vgp+2sDRJAAAAABJRU5ErkJggg==");
        return sysInfo;
    }

    /**
     * init
     */
    public static JSONObject initInfoJson() {
        return SysInfo.initInfo().toJson();
    }

    public JSONObject toJson() {
        return JSONObject.from(this);
    }

    /**
     * true 空对象
     */
    public boolean blank(){
        return StringUtils.isBlank(title) && StringUtils.isBlank(logo) && StringUtils.isBlank(favicon);
    }
}
