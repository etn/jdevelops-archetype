package ${package}.logs.entity;


import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import ${package}.jpa.convert.JsonObjectConverter;
import ${package}.logs.constant.UniqueIndexType;
import ${package}.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

/**
 * 操作审计日志
 * <p> todo 后期可以考虑每个分类[auditType]只存一年数据，大于一年放入es, 需要查询历史数据构建特殊查询</p>
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/23 09:32
 */
@Entity
@Table(name = "log_operational_audit")
@Comment("操作审计日志")
@Getter
@Setter
@ToString
@DynamicUpdate
@DynamicInsert
public class OperationalAuditLog extends SerializableBean<OperationalAuditLog> {


    /**
     * 时间戳的UUID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuidCustomGenerator")
    @GenericGenerator(name = "uuidCustomGenerator", strategy = "cn.tannn.jdevelops.jpa.generator.UuidCustomGenerator")
    @Column(columnDefinition = "bigint")
    @Comment("时间戳的UUID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;




    /**
     * 操作者登录名
     */
    @Column(columnDefinition = " varchar(50)  not null  ")
    @Comment("操作者登录名")
    @Schema(description = "操作者登录名")
    private String operatorNo;


    /**
     * 操作者姓名
     */
    @Column(columnDefinition = " varchar(200)  not null  ")
    @Comment("操作者姓名")
    @Schema(description = "操作者姓名")
    private String operatorName;


    /**
     * 操作时间
     */
    @Column(columnDefinition = " timestamp  not null  ")
    @Comment("操作时间")
    @Schema(description = "操作时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime operateTime;

    /**
     * 审计类型 {@link OperationalAuditType}
     */
    @Column(columnDefinition = " varchar(20)  not null  ")
    @Comment("审计类型[Dictionary#102]")
    @Schema(description = "审计类型[Dictionary#102]")
    private String auditType;


    /**
     * 操作类型 {@link OperationalType}
     */
    @Column(columnDefinition = " varchar(20)  not null  ")
    @Comment("操作类型[Dictionary#103]")
    @Schema(description = "操作类型[Dictionary#103]")
    private String operationalType;

    /**
     * 被操作数据的唯一值 [id/code/no,从数据源决定],[请绑定审计类型，以审计类型溯源，如跟用户相关这里就尽量写用户的唯一值]
     */
    @Column(columnDefinition = " varchar(50)  not null  ")
    @Comment(" 被操作数据的唯一值 [id/code/no,从数据源决定]")
    @Schema(description = " 被操作数据的唯一值 [id/code/no,从数据源决定][请绑定审计类型，以审计类型溯源，如跟用户相关这里就尽量写用户的唯一值]")
    private String uniqueCode;

    /**
     * 被操作数据的元数据存储索引[数据库=表名，es=索引名] {@link OperationalAuditIndex} ,[请绑定审计类型，以审计类型溯源，如跟用户相关这里就尽量写用户的唯一值]
     */
    @Column(columnDefinition = " varchar(50)  not null  ")
    @Comment("被操作数据的元数据存储索引[数据库=表名，es=索引名]")
    @Schema(description = "被操作数据的元数据存储索引[数据库=表名，es=索引名]")
    private String uniqueIndex;


    /**
     * uniqueIndex的类型
     */
    @Column(columnDefinition = " varchar(10) not null  ")
    @Comment("uniqueIndex的类型")
    @Schema(description = "uniqueIndex的类型")
    @Enumerated(EnumType.STRING)
    private UniqueIndexType uniqueIndexType;


    /**
     * 被操作数据的名字
     */
    @Column(columnDefinition = " varchar(50)")
    @Comment("被操作数据的名字")
    @Schema(description = "被操作数据的名字")
    private String dataTitle;


    /**
     * 旧数据快照[json]
     */
    @Column(columnDefinition = " json ")
    @Comment("旧数据快照[json]")
    @Schema(description = "旧数据快照[json]")
    @Convert(converter = JsonObjectConverter.class)
    private JSONObject originalData;


    /**
     * 新数据快照[json]
     */
    @Column(columnDefinition = " json")
    @Comment("新数据快照[json]")
    @Schema(description = "新数据快照[json]")
    @Convert(converter = JsonObjectConverter.class)
    private JSONObject targetData;


    /**
     * 备注
     * 参考 {@link AuditLogDescription}
     */
    @Comment("备注")
    @Column(columnDefinition = "varchar(100)")
    private String description;

}
