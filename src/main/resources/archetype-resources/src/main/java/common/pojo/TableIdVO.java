package ${package}.common.pojo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 表id处理
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/16 9:12
 */
@Getter
@Setter
@ToString
public class TableIdVO extends SerializableBean<TableIdVO> {

    /**
     * id
     */
    @Schema(description = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
}
