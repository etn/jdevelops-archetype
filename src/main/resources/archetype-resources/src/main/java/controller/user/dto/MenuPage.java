package ${package}.controller.user.dto;


import cn.tannn.jdevelops.annotations.jpa.JpaSelectIgnoreField;
import cn.tannn.jdevelops.annotations.jpa.JpaSelectOperator;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLConnect;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLOperatorWrapper;
import cn.tannn.jdevelops.jpa.request.PagingSorteds;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * 功能节点管理-查询
 *
 * @author tn
 * @version 1
 * @date 2021-06-08 09:50:02
 */
@Schema(description = "查询菜单路由")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuPage extends SerializableBean<MenuPage> {

	/**
	 * 名称，同级名称唯一
	 */
	@Schema(description = "名称")
	@JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
	private String name;

	/**
	 * 父级序号
	 */
	@Schema(description = "父级序号")
	@JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
	private String parentCode;


	/**
	 * 父级序号
	 */
	@Schema(description = "父级名称")
	@JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
	private String parentName;


	/**
	 * 节点状态
	 */
	@Schema(description = "节点状态（0、禁用；1、启用")
	@JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
	@Max(1)
	@Min(0)
	private Integer status;

	/**
	 * 分页排序
	 */
	@Schema(description = "分页排序")
	@JpaSelectIgnoreField
	@Valid
	private PagingSorteds page;


	public PagingSorteds getPage() {
		if(page == null){
			return new PagingSorteds().fixSort(0,"sort");
		}
		return page.append(0,"sort");
	}
}
