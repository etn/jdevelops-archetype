package ${package}.controller.user.dto;

import ${package}.customer.constant.LoginType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 登录附属信息
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/8/5 上午11:05
 */
@Schema(description = "登录附属信息")
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginAttach {

    /**
     * request
     */
    private HttpServletRequest request;
    /**
     * 登录类型
     */
    private LoginType loginType;
}
