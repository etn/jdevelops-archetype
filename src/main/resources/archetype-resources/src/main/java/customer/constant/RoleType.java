package ${package}.customer.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 角色类型
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 13:22
 */
@AllArgsConstructor
@Getter
public enum RoleType {

    /**
     * 内置
     */
    DEF(1, "内置"),


    /**
     * 自建
     */
    ONESELF(2, "自建"),


    ;

    private final int type;
    private final String description;


    /**
     * 获取枚举实例
     *
     * @param type int
     * @return RoleType
     */
    public static RoleType fromValue(Integer type) {
        for (RoleType roleType : RoleType.values()) {
            if (Objects.equals(type, roleType.getType())) {
                return roleType;
            }
        }
        throw new IllegalArgumentException();
    }

}
