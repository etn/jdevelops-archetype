package ${package}.controller.sys.dto;

import ${package}.util.CodeUtil;
import ${package}.sys.entity.Dictionary;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 字典表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Schema(description = "新增字典表")
@ToString
@Getter
@Setter
public class DictionaryAdd extends SerializableBean<DictionaryAdd> {

    /**
     * 自定义值[如：未知男女的标准值为：0，1，2 而我们的code是100001,100002,10003，此时这里的自定义值就用上了，此值为用户输入]
     */
    @Schema(description = "自定义值[如：未知男女的标准值为：0，1，2 而我们的code是100001,100002,10003，此时这里的自定义值就用上了，此值为用户输入]")
    private String val;

    /**
     * 名称;同级唯一
     */
    @Schema(description = "名称(同级唯一)", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String name;

    /**
     * 父级代码
     */
    @Schema(description = "父级代码,根为0", defaultValue = "0")
    private String parentCode;


    /**
     * 状态;0、不可用，1、可用，默认1
     */
    @Schema(description = "状态(0、不可用，1、可用)", defaultValue = "1")
    private Integer status;


    /**
     * 排序;越小排序越靠前，默认999
     */
    @Schema(description = "排序(越小排序越靠前，默认999)", defaultValue = "1")
    private Integer sort;

    public String getParentCode() {
        return parentCode == null ? "0" : parentCode;
    }


    public Dictionary toDictionary(String code) {
        Dictionary dictionary = new Dictionary();
        dictionary.setCode(code);
        dictionary.setName(name);
        dictionary.setParentCode(parentCode);
        dictionary.setStatus(status);
        dictionary.setSort(sort);
        dictionary.setVal(val);
        dictionary.setLevel(CodeUtil.dictCodeLevel(code));
        return dictionary;
    }
}
