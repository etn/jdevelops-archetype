package ${package}.controller.sys.dto;

import ${package}.sys.domain.SysConfigEnum;
import ${package}.sys.domain.SysInfo;
import ${package}.sys.entity.SysConfig;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 系统配置表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Schema(description = "编辑系统基础信息")
@ToString
@Getter
@Setter
public class SysConfigInfoEdit {

    /**
     * 系统标题
     */
    @Schema(description = "系统标题")
    private String title;


    /**
     * 系统logo[base64]
     */
    @Schema(description = "系统logo[base64]")
    private String logo;


    /**
     * 系统favicon[base64]
     */
    @Schema(description = "系统favicon[base64]")
    private String favicon;


    public SysConfig toSysConfig(){
        SysConfig config = new SysConfig();
        config.setCode(SysConfigEnum.SYS_INFO.getCode());
        config.setTitle(SysConfigEnum.SYS_INFO.getTitle());
        SysInfo sysInfo = new SysInfo();
        // ps，如果不修改请保持原值传回来
        sysInfo.setTitle(title);
        sysInfo.setLogo(logo);
        sysInfo.setLogo(favicon);
        config.setData(sysInfo.toJson());
        return config;
    }

}
