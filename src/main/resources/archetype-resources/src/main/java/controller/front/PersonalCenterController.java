package ${package}.controller.front;

import cn.tannn.jdevelops.annotations.web.authentication.ApiMapping;
import cn.tannn.jdevelops.annotations.web.mapping.PathRestController;
import cn.tannn.jdevelops.jwt.redis.service.RedisLoginService;
import cn.tannn.jdevelops.jwt.standalone.util.JwtWebUtil;
import ${package}.controller.front.dto.FixUserInfo;
import ${package}.controller.front.dto.PasswordEdit;
import ${package}.controller.front.vo.SelfVO;
import ${package}.customer.entity.Customer;
import ${package}.customer.service.CustomerService;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import cn.tannn.jdevelops.result.response.ResultVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

/**
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/5 16:11
 */
@PathRestController("/front/self")
@Tag(name = "个人中心")
@Slf4j
@RequiredArgsConstructor
public class PersonalCenterController {

    private final CustomerService customerService;
    private final RedisLoginService redisLoginService;

    @Operation(summary = "用户信息")
    @GetMapping(value = "info")
    public ResultVO<SelfVO> userInfo(HttpServletRequest request) {
        String tokenSubjectExpires = JwtWebUtil.getTokenSubjectExpires(request);
        Optional<Customer> byLoginName = customerService.findByLoginName(tokenSubjectExpires);
        return byLoginName.
                map(user -> ResultVO.success(SerializableBean.to2(user, SelfVO.class)))
                .orElseGet(() -> ResultVO.failMessage("获取用户失败，请尝试重新登录"));
    }

    @Operation(summary = "修改基础信息")
    @PostMapping("/fix/info")
    public ResultVO<String> userEdit(@RequestBody @Valid FixUserInfo edit, HttpServletRequest request) {
        String loginName = JwtWebUtil.getTokenSubjectExpires(request);
        Customer user = customerService.findByLoginNameTry(loginName,"请重新登录后再试");
        customerService.saveOne(edit.toSaveBean(user));
        return ResultVO.successMessage("修改成功");
    }

    /**
     * 修改密码 - 旧密码修改
     */
    @ApiMapping(value = "/fix/password",  method = RequestMethod.POST)
    @Operation(summary = "修改密码")
    @Transactional(rollbackFor = Exception.class)
    public ResultVO<String> fixPassword(@RequestBody @Valid PasswordEdit password, HttpServletRequest request) {
        String loginName = JwtWebUtil.getTokenSubjectExpires(request);
        // 被修改的账户有效性验证
        Customer user = customerService.findByLoginNameTry(loginName,"请重新登录后再试");
        // 非admin 修改 验证旧密码
        user.verifyUserPassMd5(loginName,password.getOldPassword());
        customerService.editPassword(loginName, password.getNewPassword());
        redisLoginService.loginOut(user.getLoginName());
        return ResultVO.successMessage("密码修改成功，请妥善保管新密码");
    }

}
