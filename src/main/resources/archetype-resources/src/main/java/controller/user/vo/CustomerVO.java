package ${package}.controller.user.vo;

import ${package}.common.pojo.TableIdTimeVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 用户
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-10-25
 */
@Schema(description = "用户" )
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerVO extends TableIdTimeVO<CustomerVO> {

    /** 登录名 */
    @Schema(description = "登录名")
    private  String  loginName ;



    /** 用户头像 */
    @Schema(description = "用户头像")
    private  String  icon ;


    /** 用户昵称 */
    @Schema(description = "用户昵称")
    private  String  nickname ;


    /** 姓名 */
    @Schema(description = "姓名")
    private  String  name ;


    /** 性别:0[未知]，1[男性]，2[女性] */
    @Schema(description = "性别:0[未知]，1[男性]，2[女性]")
    private  Integer gender ;

    /** 状态:1[正常],2[锁定],3[逻辑删除] */
    @Schema(description = "状态:1[正常],2[锁定],3[逻辑删除]")
    private  Integer status ;

    /**
     * 状态标记说明
     * @see UserStatusMark
     */
    @Schema(description = "状态标记说明")
    String statusMark;


    /**
     * 审核状态:1[审核中],2[审核通过],3[审核不通过]
     */
    @Schema(description = "审核状态:1[审核中],2[审核通过],3[审核不通过]")
    private Integer auditStatus;

    /**
     * 审核时间
     */
    @Schema(description = "审核时间")
    private LocalDateTime auditTime;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    /**
     * 审核备注
     */
    @Schema(description = "审核备注")
    private String auditRemark;
}
