package ${package}.util;

import ${package}.controller.logs.dto.LoginLogRecord;
import ${package}.logs.service.LoginLogService;
import cn.tannn.jdevelops.result.spring.SpringContextUtils;

/**
 * 日志
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/7/17 下午2:05
 */
public class LogUtil {

    /**
     * 记录登录日志
     * @param login LoginLogRecord
     */
    public static void loginLog(LoginLogRecord login){
        LoginLogService logService = SpringContextUtils.getInstance().getBean(LoginLogService.class);
        logService.recordLog(login);
    }

}
