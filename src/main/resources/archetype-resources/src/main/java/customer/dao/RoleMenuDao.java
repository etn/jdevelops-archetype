package ${package}.customer.dao;


import ${package}.customer.entity.RoleMenu;
import cn.tannn.jdevelops.jpa.repository.JpaBasicsRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色-菜单
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:05
 */
public interface RoleMenuDao extends JpaBasicsRepository<RoleMenu,Long> {

    /**
     * 查询功能节点序号集
     *
     * @param roleCode 角色code
     * @return menuCode
     */
    @Query("SELECT mdb.menuCode  FROM RoleMenu mdb where mdb.roleCode = ?1 ")
    List<String> findMenuCodeByRoleCode(String roleCode);

    /**
     * 查询功能节点序号集
     *
     * @param roleCode 角色code
     * @return menuCode
     */
    @Query("SELECT mdb.menuCode  FROM RoleMenu mdb where mdb.roleCode in (?1) ")
    List<String> findMenuCodeByRoleCode(List<String> roleCode);


    /**
     * 根据角色学号删除数据
     * @param roleCode 角色序号
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    void removeByRoleCode(String roleCode);


    /**
     * 根据角色学号删除数据
     * @param roleCode 角色序号
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    void removeByRoleCodeIn(List<String> roleCode);
}
