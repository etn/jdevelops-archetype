package ${package}.common.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * vo id+time
 *
 * @author tan
 * @date 2021-01-21 14:20
 */
@Getter
@Setter
@ToString
public class TableIdTimeVO<V> extends SerializableBean<V> {

    /**
     * id
     */
    @Schema(description = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 表示该字段为创建时间字段，
     */
    @Schema(description = "创建时间")
    private String createTime;

    /**
     * 表示该字段为创建人，
     */
    @Schema(description = "创建者")
    private String createUserName;


}
