package ${package}.sys.service.impl;


import cn.tannn.jdevelops.jpa.service.J2ServiceImpl;
import ${package}.sys.dao.SysConfigDao;
import ${package}.sys.domain.SysConfigEnum;
import ${package}.sys.entity.SysConfig;
import ${package}.sys.service.SysConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统配置表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Slf4j
@Service
public class SysConfigServiceImpl extends J2ServiceImpl<SysConfigDao, SysConfig, Integer> implements SysConfigService {

    public SysConfigServiceImpl() {
        super(SysConfig.class);
    }

    @Override
    public SysConfig findInfo() {
        return getJpaBasicsDao().findById(SysConfigEnum.SYS_INFO.getCode()).orElse(SysConfig.initInfo());
    }

    @Override
    public SysConfig findBeiAn() {
        return getJpaBasicsDao().findById(SysConfigEnum.BEI_AN.getCode()).orElse(SysConfig.initBeiAn());
    }
}
