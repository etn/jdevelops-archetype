package ${package}.customer.service;


import ${package}.controller.user.dto.*;
import ${package}.customer.entity.Customer;
import cn.tannn.jdevelops.jpa.service.J2Service;

import java.util.List;
import java.util.Optional;
/**
 * 系统用户(管理者)
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-10-25
 */
public interface CustomerService extends J2Service<Customer> {


    /**
     * 用户登录校验，校验成功返回用户信息
     *
     * @param login     login
     * @param attach 登录附加信息
     * @return Customer
     */
    Customer authenticateUser(List<String> platform, LoginPassword login, LoginAttach attach);


    /**
     * 注册账户 [默认状态为正常]
     *
     * @param register CustomerRegister
     * @param attach 注册内置的一些附属信息
     * @return boolean
     */
    boolean registerUser(CustomerRegister register, CustomerRegisterAttach attach);


    /**
     * 修改密码
     *
     * @param loginName   登录名
     * @param newPassword 新密码
     */
    void editPassword(String loginName, String newPassword);



    /**
     * 查询用户
     *
     * @param loginName 登录名
     * @return 用户
     */
    Optional<Customer> findByLoginName(String loginName);


    /**
     * 查询用户
     * @param loginName 登录名
     * @return 用户
     */
    Customer findByLoginNameTry(String loginName,String errorMassage);


    /**
     * 查询用户
     *
     * @param ids id
     * @return 用户
     */
    List<Customer> findByIds(List<Long> ids);

    /**
     * 判断用户是否存在
     *
     * @param loginName 登录名
     * @return true 存在
     */
    boolean userExist(String loginName);


    /**
     * 验证当前登录用户是非为超级管理员
     * @param loginName HttpServletRequest传递时有时候不安全，所以直接传值
     * @return 异常直接false
     */
    boolean verifyLoginAdmin(String loginName);


    /**
     * 设置用户状态
     *
     * @param status SettingStatus
     */
    void settingStatus(CustomerSettingStatus status);

    /**
     * 查询用户
     *
     * @param id id
     * @return 用户
     */
    Optional<Customer> findById(Long id);



}
