package ${package}.util;

import ${package}.customer.constant.MenuStatus;
import ${package}.customer.constant.MenuType;
import ${package}.customer.entity.Menu;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

/**
 * 路由jpa查询条件
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-08 17:06
 */
public final class MenuSpecification {



    /**
     * 条件  code  !=
     */
    public static Specification<Menu> neCode(String code) {
        if (ObjectUtils.isEmpty(code)) {
            return (root, criteriaQuery, criteriaBuilder) -> criteriaQuery.getRestriction();
        }else{
            return (root, query, builder) -> builder.notEqual(root.get("code"), code);
        }
    }

    /**
     * 条件  name  =
     */
    public static Specification<Menu> eqName(String name) {
        if (ObjectUtils.isEmpty(name)) {
            return (root, criteriaQuery, criteriaBuilder) -> criteriaQuery.getRestriction();
        }else{
            return (root, query, builder) -> builder.equal(root.get("name"), name);
        }
    }

    /**
     * 条件  parentCode  =
     */
    public static Specification<Menu> eqParentCode(String parentCode) {
        if (ObjectUtils.isEmpty(parentCode)) {
            return (root, criteriaQuery, criteriaBuilder) -> criteriaQuery.getRestriction();
        }else{
            return (root, query, builder) -> builder.equal(root.get("parentCode"), parentCode);
        }
    }


    /**
     * 节点状态;0、禁用；1、启用
     * 条件  status  =
     */
    public static Specification<Menu> eqStatus(MenuStatus status) {
        if (ObjectUtils.isEmpty(status)) {
            return (root, criteriaQuery, criteriaBuilder) -> criteriaQuery.getRestriction();
        }else{
            return (root, query, builder) -> builder.equal(root.get("status"), status.getStatus());
        }
    }



    /**
     * 类型;0、节点；1、功能；2、按钮
     * 条件  type  =
     */
    public static Specification<Menu> eqType(MenuType type) {
        if (ObjectUtils.isEmpty(type)) {
            return (root, criteriaQuery, criteriaBuilder) -> criteriaQuery.getRestriction();
        }else{
            return (root, query, builder) -> builder.equal(root.get("type"), type.getType());
        }
    }
}
