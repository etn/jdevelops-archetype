package ${package}.logs.service.impl;


import ${package}.controller.logs.dto.LoginLogRecord;
import ${package}.logs.dao.LoginLogDao;
import ${package}.logs.entity.LoginLog;
import ${package}.logs.service.LoginLogService;
import cn.tannn.jdevelops.jpa.service.J2ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 登录日志
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 10:55
 */
@Service
@Slf4j
public class LoginLogServiceImpl extends J2ServiceImpl<LoginLogDao, LoginLog, Long> implements LoginLogService {
    public LoginLogServiceImpl() {
        super(LoginLog.class);
    }


    @Override
    @Async("applicationTaskExecutor")
    public void recordLog(LoginLogRecord recordLog) {
        try {
            getJpaBasicsDao().save(recordLog.recordStorage());
        }catch (Exception e){
            log.error("登录日志记录失败", e);
        }
    }
}
