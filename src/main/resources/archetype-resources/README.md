# 项目名称


# 分支说明
master 主分支兼开发分支


# 配置文件使用说明
dev 开发配置文件
pord 部署配置文件

# 部署说明
> 所有sh都跟jar平级
## 特别说明
1. 复制到服务器的sh文件必须要使用 unix格式
```shell
vim xx.she
:set ff
:set ff=unix
:wq
```

## centos 服务注册说明
1. 将 script 目录下的 `xx.service` 复制到 `/etc/systemd/system/` 下
2. 修改 `xx.service` 中的属性值 
   3. WorkingDirectory 写上 sh文件的存放路径
   4. ExecStart  写上start.sh 的全路径
   5. ExecStop   写上stop.sh 的全路径
3. 修改 start.sh 中的 java环境路径 `JAVA_HOME`
4. 全部完成后开始注册和使用
```shell
# 启动命令
systemctl daemon-reload
# Created symlink
systemctl enable 项目名/项目名.service
# 操作
systemctl start 项目名/项目名.service
systemctl stop 项目名/项目名.service
systemctl status 项目名/项目名.service
# 日志查看
journalctl -u 项目名
```


## start.sh(bat) 脚本说明
> ./start.sh -f prod -p 9003
1. 查看提示 ./start.sh --?
2. 选择配置文件 ./start.sh -f prod
3. 设置启动端口 ./start.sh -p 8001


## nginx

```nginx configuration

http {
  include       mime.types;
  default_type  application/octet-stream;
  sendfile        on;
  #tcp_nopush     on;
  #keepalive_timeout  0;
  # 大小
  client_max_body_size 1024M;
  # timeout时间
  keepalive_timeout  1800;
  #gzip  on;
  server {
      listen       80;
      server_name  localhost;
      #listen   443 ssl;
      #server_name  xx.cn;
      #ssl_certificate      /usr/local/openresty/nginx/xx.cn_nginx/xx.cn.pem;
      #ssl_certificate_key  /usr/local/openresty/nginx/xx.cn_nginx/xx.tannn.cn.key;
      #ssl_session_cache    shared:SSL:1m;
      #ssl_session_timeout  5m;
      #client_max_body_size 500M;
      ## SSL Settings
      #ssl_ciphers  HIGH:!aNULL:!MD5;
      #ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
      #ssl_prefer_server_ciphers   on;
      ##  Gzip Settings
      #gzip on;
      #gzip_disable "msie6";
     # 接口地址
     location /${artifactId}-api/ {
        proxy_pass  http://localhost:9003/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_http_version 1.1;
        add_header X-Cache $upstream_cache_status;
        add_header Cache-Control no-cache;
        proxy_ssl_server_name off;
        proxy_ssl_name $proxy_host;
     }
   
   
     # 前端访问地址
     location /${artifactId} {
         alias /usr/share/nginx/html/admin;
         index index.html;
         try_files $uri $uri/ /${artifactId}/index.html;
     }
    }
}
```        


# 特别说明    

## Long类型参数前端失真问题处理
在返回的结构参数上上加入`@JsonSerialize(using = ToStringSerializer.class)`  


## ip2region 会占用很大的内存
不想开启就把`Ip2regionInit`注释了



# docker env
> 请在`application.yaml`中使用如下变量，好做部署时的动态替换

|属性| 说明       | 参考值            |
|---|----------|----------------|
|CONFIG_ENV| 配置文件选择   | dev            |
|FILE_MAX_SIZE| 单个文件大小   | 500MB          |
|FILE_MAX_REQUEST| 总上传的文件大小 | 500MB          |
|REDIS_DB| redis库   | 2              |
|REDIS_HOST| redis地址  | 127.0.0.1      |
|REDIS_PORT| redis端口  | 6379           |
|REDIS_PWD| redis密码  | 123456         |
|MYSQL_PWD| 数据库密码    | root           |
|MYSQL_UNM| 数据库账户    | root           |
|MYSQL_URL| 数据库地址    | 127.0.0.1:3306 |
|MYSQL_DB| 数据库名     | db_test        |
|ES_URIS| es地址     | 127.0.0.1:9200 |
|ES_USERNAME| es账户     | elastic        |
|ES_PASSWORD| es密码     | elastic        |
|CHAR_KEY| ai模型key  | key            |
|CHAR_MODEL| ai模型名    | glm-4-flash    |
|DOC_PASSWORD| 文档访问密码   | doc            |
|DOC_USERNAME| 文档访问账户   | doc            |


# Docker Registry
> 本地的私库访问测试：http://192.168.1.141:5001/v2/
> 查看镜像：http://192.168.1.141:5001/v2/_catalog
> 查询镜像详情：http://192.168.1.141:5001/v2/镜像名/tags/list

**注意修改下面的`app`为当前项目的名字**

## 推送到私服
1. 打标签
```shell
# tannn/app:0.0.1 -> 本地镜像
# 192.168.1.141:5001/app:0.0.1 -> 私库镜像格式（必须以私库地址为准）
docker tag tannn/app:0.0.1 192.168.1.141:5001/app:0.0.1
```
2. 推送镜像
```shell
docker push 192.168.1.141:5001/app:0.0.1
```

## 问题处理
1. Get "https://192.168.1.141:5001/v2/": http: server gave HTTP response to HTTPS client
   > 改完记得重启
   - 修改文件 vim /etc/docker/daemon.json ，增加 insecure-registries 配置，值为私有仓库的访问地址 `"insecure-registries":["192.168.1.141:5001"]`
   - window 在设置-> docker engine -> 添加 `"insecure-registries":["192.168.1.141:5001"]` 
