package ${package}.controller.user.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 登录返回
 *
 * @author tn
 * @version 1.0
 * @data 2022/12/30 15:14
 */
@Schema(description = "登录VO")
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginVO {

    @Schema(description = "token")
    private String token;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Schema(description = "手机号，只有微信登录有")
    private String phone;

    public LoginVO(String token) {
        this.token = token;
    }
}

