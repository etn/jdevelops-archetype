package ${package}.controller.user.vo;


import ${package}.common.pojo.TableIdTimeVO;
import ${package}.customer.constant.MenuStatus;
import ${package}.customer.constant.MenuType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 模块表
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-8
 */
@Schema(description = "菜单视图")
@ToString
@Getter
@Setter
@NoArgsConstructor
public class MenuVO extends TableIdTimeVO<MenuVO> {

    /**
     * 编号
     */
    @Schema(description = "编号")
    private String code;


    /**
     * 名称;同级名称唯一
     */
    @Schema(description = "名称(同级名称唯一)")
    private String name;


    /**
     * 父级序号;唯一
     */
    @Schema(description = "父级序号(唯一)")
    private String parentCode;


    /**
     * 父级名称
     */
    @Schema(description = "父级名称")
    private String parentName;


    /**
     * 排序;数字越小排在越前面
     */
    @Schema(description = "排序(数字越小排在越前面)")
    private Integer sort;


    /**
     * 前端组件;父级序号
     */
    @Schema(description = "前端组件(父级序号)")
    private String component;


    /**
     * 前端组件名称
     */
    @Schema(description = "前端组件名称")
    private String componentName;


    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;


    /**
     * 节点路由
     */
    @Schema(description = "节点路由")
    private String router;


    /**
     * 类型;0、节点；1、功能；2、按钮
     *
     * @see MenuType
     */
    @Schema(description = "类型(0、节点；1、功能；2、按钮)")
    private Integer type;


    /**
     * 节点状态;0、禁用；1、启用；默认启用
     *
     * @see MenuStatus
     */
    @Schema(description = "节点状态(0、禁用；1、启用；默认启用)")
    private Integer status;

}
