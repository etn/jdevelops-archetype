package ${package}.customer.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 用户状态常量
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 12:56
 */
@AllArgsConstructor
@Getter
public enum CustomerStatus {

    /**
     * 正常
     */
    NORMAL(1, "正常"),

    /**
     * 锁定
     */
    BANNED(2, "锁定"),

    /**
     * 删除[禁用]
     */
    DELETE(3, "逻辑删除"),
    ;

    private final int status;
    private final String description;


    /**
     * 获取枚举实例
     *
     * @param status int
     * @return CustomerStatus
     */
    public static CustomerStatus fromValue(Integer status) {
        for (CustomerStatus customerStatus : CustomerStatus.values()) {
            if (Objects.equals(status, customerStatus.getStatus())) {
                return customerStatus;
            }
        }
        throw new IllegalArgumentException();
    }
}
