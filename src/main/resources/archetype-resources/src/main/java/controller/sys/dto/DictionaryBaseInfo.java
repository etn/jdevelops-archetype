package ${package}.controller.sys.dto;

import cn.tannn.jdevelops.annotations.jpa.JpaUpdate;
import ${package}.sys.entity.Dictionary;
import ${package}.sys.service.DictionaryService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

/**
 * 修改字典基础信息
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Schema(description = "修改字典基础信息")
@ToString
@Getter
@Setter
public class DictionaryBaseInfo {

    @Schema(description = "id", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    @JpaUpdate(unique = true)
    private Integer id;

    /**
     * 名称;同级唯一
     */
    @Schema(description = "名称(同级唯一)")
    private String name;

    /**
     * 自定义值[如：未知男女的标准值为：0，1，2 而我们的code是100001,100002,10003，此时这里的自定义值就用上了，此值为用户输入]
     */
    @Schema(description = "自定义值[如：未知男女的标准值为：0，1，2 而我们的code是100001,100002,10003，此时这里的自定义值就用上了，此值为用户输入]")
    private String val;

    /**
     * 名称;同级唯一
     */
    @Schema(description = "排序;越小排序越靠前，默认999")
    private Integer sort;

    public void toSave(Dictionary dict, DictionaryService dictionaryService) {
        if (sort != null) {
            dict.setSort(sort);
        }
        if (StringUtils.isNotBlank(name) && !name.equalsIgnoreCase(dict.getName())) {
            dict.setName(name);
            dictionaryService.peerUniqueTry(dict.getParentCode(), name);
        }
        if (StringUtils.isNotBlank(val)) {
            dict.setName(val);
        }
        dictionaryService.saveOne(dict);
    }

}
