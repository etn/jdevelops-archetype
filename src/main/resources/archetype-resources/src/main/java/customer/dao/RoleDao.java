package ${package}.customer.dao;

import ${package}.customer.entity.Role;
import cn.tannn.jdevelops.jpa.repository.JpaBasicsRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 用户角色
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:02
 */
public interface RoleDao extends JpaBasicsRepository<Role,Long> {

    /**
     * 查询角色
     * @param codes 编码
     * @return roles
     */
    List<Role> findByCodeIn(List<String> codes);


    /**
     * 查询角色
     * @param code 编码
     * @return roles
     */
    Optional<Role> findByCode(String code);


    /**
     * 删除角色（自建）
     * @param codes code
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    @Query("delete from Role  re where re.code in (:codes) and re.type = 2 ")
    void deleteByCodeInAndType(@Param("codes") List<String> codes);

    /**
     * 更新状态(自建)
     *
     * @param codes codes
     * @param status status  状态;0、停用 ，1、启用
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    @Query("update Role ar set ar.status = :status where  ar.code in (:codes) and ar.type = 2")
    void changeStatus(@Param("codes") List<String> codes, @Param("status") Integer status);

    /**
     * 查询名字是否重复
     *
     * @param name name
     * @return Boolean
     */
    Boolean existsByName(String name);

    /**
     * 查询启用的角色  [状态=启用]
     * @return  CustomerRoleEntity
     */
    @Query("select r from Role r where  r.status=1")
    List<Role> findEnableRoles();

    /**
     * 查询启用的角色  [状态=启用]
     * @param codes codes
     * @return  CustomerRole
     */
    @Query("select r from Role r where r.code in (:codes) and r.status=1")
    List<Role> findEnableRoles(@Param("codes") List<String> codes);
}
