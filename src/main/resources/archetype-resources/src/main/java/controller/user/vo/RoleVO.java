package ${package}.controller.user.vo;

import ${package}.common.pojo.TableIdTimeVO;
import ${package}.customer.constant.RoleType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 角色
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 11:52
 */
@Schema(description = "角色VO")
@Getter
@Setter
@ToString
public class RoleVO extends TableIdTimeVO<RoleVO> {

    /**
     * 角色代码
     */
    @Schema(description = "角色代码")
    private String code;

    /**
     * 角色名称
     */
    @Schema(description = "角色名称")
    private String name;


    /**
     * 状态;0[停用] ，1[启用]
     *
     * @see ${package}.customer.constant.RoleStatus
     */
    @Schema(description = "状态;0[停用] ，1[启用]")
    private Integer status;


    /**
     * 角色类型：1:内置(不允许删除停用), 2:自建
     *
     * @see RoleType
     */
    @Schema(description = "角色类型：1:内置(不允许删除停用), 2:自建")
    private Integer type;


    /**
     * 排序;数字越小排在最前面
     */
    @Schema(description = "排序;数字越小排在最前面")
    private Integer sort;


    /**
     * 角色描述
     */
    @Schema(description = "角色描述")
    private String description;
}
