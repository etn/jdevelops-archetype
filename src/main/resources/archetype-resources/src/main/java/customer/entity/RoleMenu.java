package ${package}.customer.entity;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.*;

/**
 * 角色菜单关联表
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-7
 */
@Entity
@Table(name = "relation_role_menu", indexes = {
        @Index(name = "role_menu", columnList = "roleCode,menuCode", unique = true)
})
@Comment("角色-菜单关联")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
@DynamicInsert
public class RoleMenu extends SerializableBean<RoleMenu> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "bigint")
    @Comment("主键，自动生成")
    private Long id;

    /**
     * 角色代码
     */
    @Comment("角色代码")
    @Column(columnDefinition = "varchar(50)  not null")
    private String roleCode;


    /**
     * 菜单编号
     */
    @Comment("菜单编号")
    @Column(columnDefinition = "varchar(50)  not null")
    private String menuCode;

}
