package ${package}.controller.front.vo;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 用户
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-10-25
 */
@Schema(description = "个人中心-用户信息展示")
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SelfVO extends SerializableBean<SelfVO> {

    /**
     * 登录名
     */
    @Schema(description = "登录名")
    private String loginName;


    /**
     * 用户头像
     */
    @Schema(description = "用户头像")
    private String icon;

    /**
     * 姓名
     */
    @Schema(description = "用户姓名")
    private String name;

    /**
     * 用户昵称
     */
    @Schema(description = "用户昵称")
    private String nickname;


    /**
     * 性别:0[未知]，1[男性]，2[女性]
     */
    @Schema(description = "性别:0[未知]，1[男性]，2[女性]")
    private Integer gender;

}
