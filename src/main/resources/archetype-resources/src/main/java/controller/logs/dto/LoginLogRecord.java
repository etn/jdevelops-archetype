package ${package}.controller.logs.dto;

import ${package}.customer.constant.LoginType;
import ${package}.customer.entity.Customer;
import ${package}.logs.entity.LoginLog;
import cn.tannn.ip2region.sdk.IpRegionUtil;
import cn.tannn.ip2region.sdk.UserAgentUtil;
import cn.tannn.jdevelops.utils.http.IpUtil;
import cn.tannn.jdevelops.utils.jwt.core.JwtService;
import cn.tannn.jdevelops.utils.time.TimeUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import jakarta.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 登录日志记录
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 11:28
 */
@Getter
@Setter
@ToString
@Slf4j
public class LoginLogRecord {

    /**
     * 登录者
     */
    private Customer customer;
    /**
     * 登录token
     */
    private String token;

    /**
     * 请求头
     */
    private HttpServletRequest request;

    /**
     * 请求ip, 默认是从 request里拿的，但是有时候ip会被消费掉（ The request object has been recycled and is no longer associated with this facade）
     */
    private  String ip;

    /**
     * 登录状态：0[失败],1[成功]
     */
    private Integer status;

    /**
     * 备注
     */
    private String description;

    /**
     * 登录类型
     *
     * @see LoginType
     */
    private String type;


    /**
     * 登录平台 (默认冲token里取)
     */
    private String tokenPlatform;


    public LoginLog recordStorage(){
        LoginLog recordLog = new LoginLog();
        if(ip == null){
            ip = IpUtil.getPoxyIpEnhance(request);
        }
        recordLog.setUserAgent(UserAgentUtil.parseStr(request));
        recordLog.setIpAddress(ip);
        recordLog.setIpRegion(IpRegionUtil.getIpRegion(ip));
        recordLog.setLoginTime(TimeUtil.thisDefNow());
        if (null != customer) {
            recordLog.setUserName(customer.getName());
            recordLog.setLoginName(customer.getLoginName());
            recordLog.setUserId(customer.getId());
        }
        recordLog.setStatus(status);
        recordLog.setToken(token);
        if (tokenPlatform != null) {
            if (token != null) {
                try {
                    tokenPlatform = String.join(",", JwtService
                            .getPlatformConstantExpires(token));
                } catch (Exception e) {
                    log.warn("token platform 获取失败", e);
                }
            }
        }
        recordLog.setPlatform(tokenPlatform);
        recordLog.setType(type);
        recordLog.setDescription(description);
        return recordLog;
    }


    /**
     * 自定义  Customer
     * @param loginName 登录名
     * @param userName 用户名
     */
    public void customerUser(String loginName,String userName) {
        Customer customer = new Customer();
        customer.setLoginName(loginName);
        customer.setName(userName);
        this.customer = customer;
    }

    public void writeTokenPlatform(List<String> platform) {
        if(platform != null && !platform.isEmpty()) {
            this.tokenPlatform = String.join(",", platform);
        }
    }

}

