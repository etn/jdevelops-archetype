package ${package}.sys.domain;

import com.alibaba.fastjson2.JSONObject;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 系统基础信息
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/6 10:16
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "系统备案信息")
public class SysBeiAn {

    /**
     * 备案信息
     */
    @Schema(description = "备案信息")
    private String description;


    /**
     * init
     */
    public static SysBeiAn initBeiAn() {
        SysBeiAn beiAn = new SysBeiAn();
        beiAn.setDescription("备案号：渝ICP备17012494号-1");
        return beiAn;
    }

    /**
     * init
     */
    public static JSONObject initBeiAnJson() {
        return SysBeiAn.initBeiAn().toJson();
    }

    public JSONObject toJson() {
        return JSONObject.from(this);
    }
}
