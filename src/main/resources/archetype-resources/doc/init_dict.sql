-- 初始化字典
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:43:05', 'admin', '2024-12-04 16:43:05', 'admin', '100', '用户状态', '0', 1, 1, NULL, 1);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:44:23', 'admin', '2024-12-04 16:44:23', 'admin', '100001', '正常', '100', 1, 1, '1', 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:44:36', 'admin', '2024-12-04 16:44:36', 'admin', '100002', '锁定', '100', 1, 1, '2', 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:44:36', 'admin', '2024-12-04 16:44:36', 'admin', '100003', '逻辑删除', '100', 1, 1, '3', 2);



INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:43:10', 'admin', '2024-12-04 16:43:10', 'admin', '101', '性别', '0', 1, 1, NULL, 1);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:45:53', 'admin', '2024-12-04 16:45:53', 'admin', '101001', '男', '101', 1, 1, '1', 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '101002', '女', '101', 1, 1, '2', 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '101003', '未知', '101', 1, 1, '0', 2);




INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:43:10', 'admin', '2024-12-04 16:43:10', 'admin', '102', '审计类型', '0', 1, 1, NULL, 1);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:45:53', 'admin', '2024-12-04 16:45:53', 'admin', '102001', '用户', '102', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '102002', '角色', '102', 1, 1, NULL, 2);




INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:43:10', 'admin', '2024-12-04 16:43:10', 'admin', '103', '操作类型', '0', 1, 1, NULL, 1);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:45:53', 'admin', '2024-12-04 16:45:53', 'admin', '103001', '新增', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103002', '修改', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103003', '删除', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103004', '查询', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103005', '下载', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103006', '上传', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103007', '审核', '103', 1, 1, NULL, 2);
INSERT INTO `sys_dictionary` ( `create_time`, `create_user_name`, `update_time`, `update_user_name`, `code`, `name`, `parent_code`, `sort`, `status`, `val`, `level`) VALUES ('2024-12-04 16:46:03', 'admin', '2024-12-04 16:46:03', 'admin', '103008', '驳回', '103', 1, 1, NULL, 2);



