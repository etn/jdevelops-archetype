package ${package}.controller.user.dto;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

/**
 * 系统用户表
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-10-25
 */
@Schema(description = "注册系统用户")
@ToString
@Getter
@Setter
@Valid
public class CustomerRegister extends SerializableBean<CustomerRegister> {

    /**
     * 登录名
     */
    @Schema(description = "登录名", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String loginName;


    /**
     * 登录密码
     */
    @Schema(description = "登录密码", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String password;

    /**
     * 用户昵称
     */
    @Schema(description = "用户昵称")
    private String nickname;


    /**
     * 姓名
     */
    @Schema(description = "姓名", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String name;


    /**
     * 性别;0[未知]，1[男性]，2[女性]
     */
    @Schema(description = "性别(0[未知]，1[男性]，2[女性])")
    @Min(0)
    @Max(2)
    private Integer gender;


    /**
     * 用户头像
     */
    @Schema(description = "用户头像")
    private String icon;


    public @NotBlank String getLoginName() {
        return loginName.trim();
    }

    public @NotBlank String getPassword() {
        return password.trim();
    }
}
