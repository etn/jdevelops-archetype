package ${package}.controller.user.dto;


import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;

/**
 * 功能节点管理-修改
 * @date  2021-06-08 09:50:02
 * @author tn
 * @version 1
 */
@Schema(description = "编辑菜单路由")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuAlter extends SerializableBean<MenuAlter> {

    /** 编号 */
    @Schema(description = "编号", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String code;

    /**
     * 名称，同级名称唯一
     */
    @Schema(description = "名称：修改名称时，必须要传入父级ID，如果改为顶级则传0")
    private String name;

    /**
     * 类型（0、节点；1、功能；2、按钮）
     */
    @Schema(description = "类型（0、节点；1、功能；2、按钮）")
    @Min(0)
    @Max(2)
    private Integer type;

    /**
     * 节点路由
     */
    @Schema(description = "节点路由")
    private String router;

    /**
     * 父级序号
     */
    @Schema(description = "父级序号")
    private String parentCode;

    /**
     * 排序，数字越小排在越前面
     */
    @Schema(description = "排序，数字越小排在越前面，默认0")
    private Integer sort;

    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;

    /**
     * 节点状态
     */
    @Schema(description = "节点状态（0、禁用；1、启用")
    @Min(0)
    @Max(2)
    private Integer status;

    /**
     * 前端组件
     */
    @Schema(description = "前端组件", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String component;

    /**
     * 前端组件名称
     */
    @Schema(description = "前端组件名称", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String componentName;

}
