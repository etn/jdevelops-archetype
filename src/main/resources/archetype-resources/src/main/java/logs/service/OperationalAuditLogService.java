package ${package}.logs.service;


import ${package}.jpa.service.J2Service;
import ${package}.logs.entity.OperationalAuditLog;

/**
 * 数据审计日志
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/23 09:47
 */
public interface OperationalAuditLogService extends J2Service<OperationalAuditLog> {

}
