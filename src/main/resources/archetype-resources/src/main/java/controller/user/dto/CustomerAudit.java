package ${package}.controller.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * 审核用户
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/7/29 上午11:41
 */
@Schema(description = "审核用户")
@ToString
@Getter
@Setter
public class CustomerAudit {

    /**
     * 用户id
     */
    @Schema(description = "用户id", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    List<Long> id;

    /**
     * 审核状态:2[审核通过],3[审核不通过]
     */
    @Schema(description = "审核状态:2[审核通过],3[审核不通过]")
    @Min(2)
    @Max(3)
    @NotNull
    private Integer auditStatus;

    /**
     * 审核备注
     */
    @Schema(description = "审核备注")
    private String auditRemark;


    public String getAuditRemark() {
        if (StringUtils.isBlank(auditRemark)) {
            if (auditStatus == 2) {
                return "审核通过";
            } else if (auditStatus == 3) {
                return "审核驳回";
            }
        }
        return auditRemark;
    }

    public int status() {
        return auditStatus == 2 ? 1 : 2;
    }
}
