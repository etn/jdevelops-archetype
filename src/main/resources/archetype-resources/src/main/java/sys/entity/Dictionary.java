package ${package}.sys.entity;

import cn.tannn.jdevelops.jpa.modle.basics.JpaAuditFields;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * 字典表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Entity
@Table(name = "sys_dictionary")
@Comment("字典表")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
@DynamicInsert
public class Dictionary extends JpaAuditFields<Dictionary> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int")
    @Comment("id")
    private Integer id;


    /**
     * 代码;同级唯一
     */
    @Column(columnDefinition = " varchar(200)  not null  ")
    @Comment("代码[同级唯一]")
    private String code;

    /**
     * 自定义值[如：未知男女的标准值为：0，1，2 而我们的code是100001,100002,10003，此时这里的自定义值就用上了，此值为用户输入]
     */
    @Column(columnDefinition = " varchar(200) ")
    @Comment("自定义值[如：未知男女的标准值为：0，1，2 而我们的code是100001,100002,10003，此时这里的自定义值就用上了，此值为用户输入]")
    private String val;

    /**
     * 名称;同级唯一
     */
    @Column(columnDefinition = " varchar(200)  not null  ")
    @Comment("名称[同级唯一]")
    private String name;


    /**
     * 父级代码
     */
    @Column(columnDefinition = " varchar(200) default 0 ")
    @Comment("父级代码")
    private String parentCode;


    /**
     * 状态;0、不可用，1、可用，默认1
     */
    @Column(columnDefinition = " int   default 1")
    @Comment("状态[0、不可用，1、可用]")
    private Integer status;


    /**
     * 排序;越小排序越靠前，默认999
     */
    @Column(columnDefinition = " int default 999")
    @Comment("排序[越小排序越靠前]")
    private Integer sort;

    /**
     * 层级[1-n]
     */
    @Column(columnDefinition = " int  default 1")
    @Comment("层级[1-n]")
    private Integer level;

}
