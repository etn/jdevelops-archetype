package ${package}.customer.dao;


import ${package}.customer.entity.Menu;
import cn.tannn.jdevelops.jpa.repository.JpaBasicsRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 菜单
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:02
 */
public interface MenuDao extends JpaBasicsRepository<Menu,Long> {

    /**
     * 更新状态
     * @param codes codes
     * @param status status  节点状态;0、禁用；1、启用
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    @Query("update Menu sm set sm.status = :status where  sm.code in (:codes) ")
    void changeStatus(@Param("codes") List<String> codes , @Param("status") Integer status);

    /**
     * 查询指定no的所有子级
     *
     * @param codes codes
     * @return List no
     */
    @Query("SELECT mdb.code FROM Menu mdb where mdb.parentCode in (:codes)")
    List<String> findChild(@Param("codes") List<String> codes);

    /**
     * 查询指定no的所有父级
     *
     * @param codes codes
     * @return List
     */
    @Query("SELECT mdb.parentCode FROM Menu mdb where mdb.code in (:codes)")
    List<String> findParen(@Param("codes") List<String> codes);


    /**
     * 获取  不等于 指定状态的所有节点
     *
     * @param status 节点状态;0、禁用；1、启用
     * @return Menu of List
     */
    List<Menu> findAllByStatusNot(Integer status);

    /**
     * 查询路由
     * @param code code
     * @return Menu
     */
    Optional<Menu> findByCode(String code);


    /**
     * 查询路由
     * @param codes codes
     * @return Menu
     */
    List<Menu> findByCodeIn(List<String> codes);


    /**
     * 删除路由
     * @param codes 路由编码
     */
    @Transactional(rollbackFor = Exception.class)
    @Modifying
    void deleteByCodeIn(List<String> codes);


    /**
     * 查询路由
     * @param status status 节点状态;0、禁用；1、启用
     * @param codes 路由编码
     * @return Menu of List
     */
    List<Menu> findByStatusAndCodeIn(Integer status,List<String> codes);

}
