package ${package}.customer.entity;


import ${package}.common.pojo.JpaCommonBean;
import ${package}.customer.constant.RoleStatus;
import ${package}.customer.constant.RoleType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;

/**
 * 用户角色
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 11:52
 */
@Entity
@Table(name = "tb_role",indexes = {
        @Index(name = "code_index",columnList = "code",unique = true),
        @Index(name = "name_index",columnList = "name",unique = true),
})
@Comment("角色")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
@DynamicInsert
public class Role extends JpaCommonBean<Role> {

    /**
     * 角色代码
     */
    @Comment("角色代码")
    @Column(columnDefinition = "varchar(50)  not null")
    private String code;

    /**
     * 角色名称
     */
    @Comment("角色名称")
    @Column(columnDefinition = "varchar(100)  not null")
    private String name;


    /**
     * 状态;0[停用] ，1[启用]
     * @see RoleStatus
     */
    @Comment("状态;0[停用] ，1[启用]")
    @Column(columnDefinition = "int default 1")
    private Integer status;


    /**
     * 角色类型：1:内置(不允许删除停用), 2:自建
     * @see RoleType
     */
    @Comment("角色类型：1:内置(不允许删除停用), 2:自建")
    @Column(columnDefinition = "int default 2")
    private Integer type;


    /**
     * 排序;数字越小排在最前面
     */
    @Comment("排序;数字越小排在最前面")
    @Column(columnDefinition = "int default 0")
    private Integer sort;


    /**
     * 角色描述
     */
    @Comment("角色描述")
    @Column(columnDefinition = "varchar(200)")
    private String description;
}
