package ${package}.sys.service;

import cn.tannn.jdevelops.jpa.service.J2Service;
import ${package}.sys.entity.SysConfig;

/**
 * 系统配置表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
public interface SysConfigService extends J2Service<SysConfig> {

    /**
     * 查询系统基础信息
     */
    SysConfig findInfo();

    /**
     * 查询系统备案信息
     */
    SysConfig findBeiAn();

}
