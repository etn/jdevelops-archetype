package ${package}.initialize;

import cn.tannn.jdevelops.knife4j.core.entity.BuildSecuritySchemes;
import cn.tannn.jdevelops.knife4j.domain.SwaggerProperties;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static cn.tannn.jdevelops.knife4j.core.util.SwaggerUtil.buildSecuritySchemes;

/**
 * 接口分组
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-03-12 21:05
 */
@Component
public class GroupedOpenApis {

    @Bean
    public GroupedOpenApi userapi(SwaggerProperties swaggerProperties) {
        // 加入  Authorize
        BuildSecuritySchemes buildSecuritySchemes = buildSecuritySchemes(swaggerProperties.getSwaggerSecuritySchemes());
        //  表示 packagesToScan下的所有接口
        String[] paths = {"/**"};
        // 需要扫描的包路径
        String[] packagedToMatch = {"${package}.controller.user"};

        return GroupedOpenApi.builder().group("a-userapi")
                .displayName("用户管理")
                .pathsToMatch(paths)
                .addOperationCustomizer((operation, handlerMethod) -> operation.security(buildSecuritySchemes.getSecurityItem()))
                .packagesToScan(packagedToMatch).build();
    }

    @Bean
    public GroupedOpenApi logapi(SwaggerProperties swaggerProperties) {
        // 加入  Authorize
        BuildSecuritySchemes buildSecuritySchemes = buildSecuritySchemes(swaggerProperties.getSwaggerSecuritySchemes());
        //  表示 packagesToScan下的所有接口
        String[] paths = {"/**"};
        // 需要扫描的包路径
        String[] packagedToMatch = {"${package}.controller.logs"};

        return GroupedOpenApi.builder().group("z-logapi")
                .displayName("日志管理")
                .pathsToMatch(paths)
                .addOperationCustomizer((operation, handlerMethod) -> operation.security(buildSecuritySchemes.getSecurityItem()))
                .packagesToScan(packagedToMatch).build();
    }

}
