package ${package}.sys.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 系统设置项枚举
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/6 10:26
 */
@AllArgsConstructor
@Getter
public enum SysConfigEnum {

    SYS_INFO(1, "系统基础信息"),
    BEI_AN(2, "系统备案信息");

    private final int code;
    private final String title;

    /**
     * 获取枚举实例
     * @param code int
     * @return SysConfigEnum
     */
    public static SysConfigEnum fromValue(Integer code) {
        for (SysConfigEnum configEnum : SysConfigEnum.values()) {
            if (Objects.equals(code, configEnum.getCode())) {
                return configEnum;
            }
        }
        return null;
    }

}
