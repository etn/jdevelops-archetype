package ${package}.customer.constant;

import java.util.Arrays;
import java.util.List;

/**
 * 默认角色
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/7/11 9:09
 */
public interface RoleDef {

    /**
     * 管理员
     */
    String USER_ADMIN = "admin";

    /**
     * 普通用户
     */
    String USER_COMMON = "common";


    /**
     * 不能用户注册的角色 和 编辑
     */
    List<String> NO_REGIST_ROLE = Arrays.asList(USER_ADMIN, USER_COMMON);
}
