package ${package}.customer.service;


import ${package}.controller.user.dto.RoleAdd;
import ${package}.customer.entity.Role;
import cn.tannn.jdevelops.jpa.service.J2Service;

import java.util.List;
import java.util.Optional;

/**
 * 角色
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:08
 */
public interface RoleService  extends J2Service<Role> {
    /**
     * 角色注册
     * @param authRole RoleAdd
     * @return boolean
     */
    boolean registerRole(RoleAdd authRole);

    /**
     * 判处用户是否存在
     * @param code code
     * @param name name
     * @return true 存在
     */
    boolean verifyRoleExist(final String code,final String name);

    /**
     * 判处用户是否存在
     * @param name 名字
     * @return true 存在
     */
    boolean verifyRoleExist(final String name);

    /**
     * 启用角色
     * @param codes roleCodes
     */
    void enable(List<String> codes);

    /**
     * 禁用角色 (内建角色不允许操作)
     * @param codes roleCodes
     */
    void disabled(List<String> codes);

    /**
     * 删除角色 (内建角色不允许操作)
     * @param codes codes
     */
    void deleteByCodes(List<String> codes);


    /**
     * 查询角色
     * @param code code
     * @return CustomerRole
     */
    Optional<Role> findByCode(String code);

    /**
     * 查询角色
     * @param code code
     * @return CustomerRole
     */
    List<Role> findByCode(List<String> code);

    /**
     * 查询已经启用的角色
     * @return CustomerRole
     */
    List<Role> findEnable();
}
