package ${package}.customer.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 菜单类型
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 13:21
 */
@AllArgsConstructor
@Getter
public enum MenuType {


    /**
     * 节点
     */
    NODE(0,"节点"),


    /**
     * 功能
     */
    FN(1,"功能"),

    /**
     * 按钮
     */
    BUTTON(2,"按钮"),


    ;

    private final int type;
    private final String description;


    /**
     * 获取枚举实例
     * @param type int
     * @return MenuType
     */
    public static MenuType fromValue(Integer type) {
        for (MenuType menuType : MenuType.values()) {
            if (Objects.equals(type, menuType.getType())) {
                return menuType;
            }
        }
        return null;
//        throw new IllegalArgumentException();
    }
}
