package ${package}.controller.user.dto;


import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * 系统用户设置角色
 *
 * @author tn
 * @date 2021-06-10 16:03
 */
@Schema(description = "系统用户设置角色")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleUser extends SerializableBean<RoleUser> {
	/**
	 * 角色编号
	 */
	@Schema(description = "角色编号", requiredMode = Schema.RequiredMode.REQUIRED)
	@NotNull
	private String roleCode;

	/**
	 * 用户id集合
	 */
	@Schema(description = "用户id集合", requiredMode = Schema.RequiredMode.REQUIRED)
	private List<Long> userIds;
}
