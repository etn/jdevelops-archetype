package ${package}.controller.front.dto;

import ${package}.customer.entity.Customer;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

/**
 * 编辑用户
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/11/21 9:56
 */
@Schema(description = "编辑用户")
@ToString
@Getter
@Setter
public class FixUserInfo extends SerializableBean<FixUserInfo> {

    /** 用户姓名 */
    @Schema(description = "用户姓名")
    private  String  name ;

    /** 性别;0[未知]，1[男性]，2[女性] */
    @Schema(description = "性别(0[未知]，1[男性]，2[女性])")
    @Min(0)
    @Max(2)
    private  Integer gender ;

    /**
     * 用户昵称
     */
    @Schema(description = "用户昵称")
    private String nickname;


    /**
     * @return 修改bean -> 真实bean
     */
    public Customer toSaveBean(Customer old){
        if(StringUtils.isNotBlank(name)){
            old.setName(name);
        }
        if(gender != null){
            old.setGender(gender);
        }
        if(nickname != null){
            old.setNickname(nickname);
        }
        return old;
    }

}
