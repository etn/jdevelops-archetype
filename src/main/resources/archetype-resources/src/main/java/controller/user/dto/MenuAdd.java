package ${package}.controller.user.dto;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * 功能节点管理-新增
 * @date  2021-06-08 09:50:02
 * @author tn
 * @version 1
 */
@Schema(description = "新增菜单路由")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuAdd extends SerializableBean<MenuAdd> {

	@Schema(description = "菜单编码")
	private String code;

	/**
	 * 名称，同级名称唯一
	 */
	@Schema(description = "菜单名", requiredMode = Schema.RequiredMode.REQUIRED)
	@NotBlank
	private String name;


	/**
	 * 父级序号
	 */
	@Schema(description = "父级序号")
	private String parentCode;


	/**
	 * 排序，数字越小排在越前面
	 */
	@Schema(description = "排序，数字越小排在越前面，默认0")
	private Integer sort;


	/**
	 * 前端组件
	 */
	@Schema(description = "前端组件", requiredMode = Schema.RequiredMode.REQUIRED)
	@NotBlank
	private String component;

	/**
	 * 前端组件名称
	 */
	@Schema(description = "前端组件名称", requiredMode = Schema.RequiredMode.REQUIRED)
	@NotBlank
	private String componentName;



	/**
	 * 图标
	 */
	@Schema(description = "图标")
	private String icon;

	/**
	 * 节点路由
	 */
	@Schema(description = "节点路由")
	private String router;

	/**
	 * 类型（0、节点；1、功能；2、按钮）
	 */
	@Schema(description = "类型（0、节点；1、功能；2、按钮）", requiredMode = Schema.RequiredMode.REQUIRED)
	@NotNull
	@Min(0)
	@Max(2)
	private Integer type;

	/**
	 * 节点状态
	 */
	@Schema(description = "节点状态（0、禁用；1、启用，不填默认启用", requiredMode = Schema.RequiredMode.REQUIRED)
	@Min(0)
	@Max(2)
	private Integer status;


}
