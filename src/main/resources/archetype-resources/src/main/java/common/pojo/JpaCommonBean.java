package ${package}.common.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import cn.tannn.jdevelops.jpa.modle.basics.JpaAuditFields;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;


import jakarta.persistence.*;
import jakarta.persistence.AccessType;


/**
 * 公共的实体类
 * @author tn
 * @date 2021-01-21 14:20
 */
@MappedSuperclass
@DynamicInsert
@DynamicUpdate
@SelectBeforeUpdate
@Access(AccessType.FIELD)
@Getter
@Setter
public class JpaCommonBean<B> extends JpaAuditFields<B> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuidCustomGenerator")
    @GenericGenerator(name = "uuidCustomGenerator", strategy = "cn.tannn.jdevelops.jpa.generator.UuidCustomGenerator")
    @Column(columnDefinition="bigint")
    @Comment("uuid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @Override
    public String toString() {
        return "CommonBean{" +
               "id=" + id +
               '}';
    }
}

