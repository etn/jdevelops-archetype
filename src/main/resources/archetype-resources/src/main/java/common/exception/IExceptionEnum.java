package ${package}.common.exception;

import cn.tannn.jdevelops.result.exception.ExceptionCode;

/**
 * 自定义异常
 *
 * @author tn
 * @date 2021-06-08 16:37
 */
public class IExceptionEnum {

    /**
     * 系统错误
     */
    public static final ExceptionCode PARAMETER_ERROR = new ExceptionCode(500, "系统错误");


}
