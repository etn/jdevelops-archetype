package ${package}.controller.sys;


import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import cn.tannn.jdevelops.annotations.web.authentication.ApiMapping;
import cn.tannn.jdevelops.annotations.web.authentication.ApiPlatform;
import cn.tannn.jdevelops.annotations.web.constant.PlatformConstant;
import cn.tannn.jdevelops.annotations.web.mapping.PathRestController;
import ${package}.controller.sys.dto.SysConfigBeiAnEdit;
import ${package}.controller.sys.dto.SysConfigInfoEdit;
import ${package}.sys.entity.SysConfig;
import ${package}.sys.service.SysConfigService;
import cn.tannn.jdevelops.result.response.ResultVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 系统配置表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@PathRestController("sysConfig")
@Slf4j
@Tag(name = "系统配置管理")
@RequiredArgsConstructor
public class SysConfigController {

    private final SysConfigService sysConfigService;


    @ApiMapping(value = "/info",checkToken = false)
    @Operation(summary = "查询系统基础信息", description = "info")
    @ApiOperationSupport(order = 1)
    public ResultVO<SysConfig> info() {
        SysConfig bean = sysConfigService.findInfo();
        return ResultVO.success(bean);
    }

    @Operation(summary = "修改系统基础信息")
    @PostMapping("fixInfo")
    @ApiPlatform(platform = PlatformConstant.WEB_ADMIN)
    @ApiOperationSupport(order = 2)
    public ResultVO<String> fixInfo(@RequestBody @Valid SysConfigInfoEdit edit) {
        sysConfigService.saveOne(edit.toSysConfig());
        return ResultVO.success();
    }


    @ApiMapping(value = "/beiAn",checkToken = false)
    @Operation(summary = "查询系统备案信息", description = "beiAn")
    @ApiOperationSupport(order = 3)
    public ResultVO<SysConfig> beiAn() {
        SysConfig bean = sysConfigService.findBeiAn();
        return ResultVO.success(bean);
    }


    @Operation(summary = "修改系统备案信息")
    @PostMapping("fixBeiAn")
    @ApiPlatform(platform = PlatformConstant.WEB_ADMIN)
    @ApiOperationSupport(order = 4)
    public ResultVO<String> fixBeiAn(@RequestBody @Valid SysConfigBeiAnEdit edit) {
        sysConfigService.saveOne(edit.toSysConfig());
        return ResultVO.success();
    }


}
