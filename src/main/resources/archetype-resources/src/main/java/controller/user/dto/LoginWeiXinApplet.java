package ${package}.controller.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 微信小程序登录
 *
 * @author lxw
 * @version V1.0
 * @date 2022-12-29 10:12
 */
@Schema(description = "微信小程序登录")
@ToString
@Getter
@Setter
public class LoginWeiXinApplet implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotBlank
    @Schema(description  = "加密数据", requiredMode = Schema.RequiredMode.REQUIRED)
    private String encryptedData;

    @NotBlank
    @Schema(description  = "密文串", requiredMode = Schema.RequiredMode.REQUIRED)
    private String iv;

    @NotBlank
    @Schema(description  = "openId", requiredMode = Schema.RequiredMode.REQUIRED)
    private String openId;

    @Schema(description  = "微信昵称")
    private String nickName;

    @Schema(description  = "微信头像")
    private String userIcon;

    @Schema(description  = "用户性别")
    private Integer gender;

    public Integer getGender() {
        // 默认未知
        return null == gender ? 0 :gender;
    }
}
