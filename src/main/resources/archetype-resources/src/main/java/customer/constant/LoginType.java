package ${package}.customer.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录类型
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 12:57
 */
@AllArgsConstructor
@Getter
public enum LoginType {
    /**
     * 账号密码登录
     */
    WEB_ACCOUNT_PASSWORD("利用端账号密码"),

    /**
     * 账号密码登录
     */
    WEB_ACCOUNT_IP("利用端IP登录"),

    /**
     * 账号密码登录
     */
    ADMIN_ACCOUNT_PASSWORD("管理端账号密码"),

    /**
     * 微信小程序
     */
    WECHAT_APPLET("微信小程序");

    private final String description;
}
