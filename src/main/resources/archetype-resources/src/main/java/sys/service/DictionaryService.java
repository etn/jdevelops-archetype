package ${package}.sys.service;

import cn.hutool.core.lang.tree.Tree;
import ${package}.sys.entity.Dictionary;
import cn.tannn.jdevelops.jpa.service.J2Service;

import java.util.List;

/**
 * 字典表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
public interface DictionaryService extends J2Service<Dictionary> {
    /**
     * 获取下一个code
     *
     * @param parentCode 父级code, 为空时，从根节点开始
     * @return code+1
     */
    String nextCode(String parentCode);

    /**
     * 查询单层子集
     *
     * @param parentCode 父级code
     * @return Dictionary
     */
    List<Dictionary> findByParentCode(String parentCode);

    /**
     * 同级唯一查询
     *
     * @param parentCode 父级code
     * @param name       name
     * @return true唯一
     */
    boolean peerUnique(String parentCode, String name);

    /**
     * 同级唯一查询 [不唯一会抛出异常]
     *
     * @param parentCode 父级code
     * @param name       name
     */
    void peerUniqueTry(String parentCode, String name);


    /**
     * 修改排序
     *
     * @param id   id
     * @param sort sort
     */
    void fixSort(Integer id, Integer sort);


    /**
     * 获取有效的字典列表
     *
     * @param parentCode 父级code 不传查询就不带他
     * @return Dictionary
     */
    List<Dictionary> findEfficacious(String parentCode);

    /**
     * 获取字典列表
     *
     * @param parentCode 父级code 不传查询就不带他
     * @return Dictionary
     */
    List<Dictionary> finds(String parentCode);

    /**
     * 启用 - 启用会启用他的所有父类
     *
     * @param code code
     */
    void enable(String code);

    /**
     * 禁用字典 - 禁用会禁用他的所有子类，禁用不会干扰原来已经用的字典
     *
     * @param code code
     */
    void disabled(String code);

    /**
     * 字典树
     *
     * @return Tree
     */
    List<Tree<String>> tree();


    /**
     * 获取所有子级
     * @param code code
     * @return 所有子级
     */
    List<Dictionary> findLowLevel(String code);
}
