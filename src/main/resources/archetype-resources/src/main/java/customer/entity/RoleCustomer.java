package ${package}.customer.entity;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.*;

/**
 * 角色-用户
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 13:25
 */
@Entity
@Table(name = "relation_customer_role", indexes = {
        @Index(name = "role_customer",columnList = "roleCode,userId",unique = true)
})
@Comment("角色-系统用户关联")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
@DynamicInsert
public class RoleCustomer extends SerializableBean<RoleCustomer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "bigint")
    @Comment("主键，自动生成")
    private Long id;


    /**
     * 角色代码
     */
    @Comment("角色代码")
    @Column(columnDefinition = "varchar(50)  not null")
    private String roleCode;


    /**
     * 系统用户
     */
    @Comment("系统用户")
    @Column(columnDefinition = "bigint not null")
    private Long userId;
}
