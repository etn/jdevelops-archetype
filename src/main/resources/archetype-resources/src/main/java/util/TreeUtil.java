package ${package}.util;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import ${package}.customer.entity.Menu;
import ${package}.sys.entity.Dictionary;

import java.util.List;

/**
 * 树处理
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/10/10 上午9:49
 */
public class TreeUtil {

    /**
     * 字典树
     * @param dictionaries dictionaries
     * @return Tree<String>
     */
    public static List<Tree<String>> dictTree(List<Dictionary> dictionaries){
        //配置
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        // 自定义属性名 都要默认值的
        treeNodeConfig.setWeightKey("sortNumber");
        // 最大递归深度
        treeNodeConfig.setDeep(10);
        //转换器
        return cn.hutool.core.lang.tree.TreeUtil.build(dictionaries, "0", treeNodeConfig,
                (treeNode, tree) -> {
                    tree.setId(treeNode.getCode());
                    tree.setParentId(treeNode.getParentCode());
                    tree.setWeight(treeNode.getSort());
                    tree.setName(treeNode.getName());
                    // 扩展属性 ...
                    tree.putExtra("code", treeNode.getCode());
                    tree.putExtra("sourceId", treeNode.getId()+"");
                    tree.putExtra("status", treeNode.getStatus());
                    tree.putExtra("val", treeNode.getVal());
                });
    }


    /**
     * 菜单树
     * @param menus Menu
     * @return Tree<String>
     */
    public static List<Tree<String>> menusTree(List<Menu> menus) {
        //配置
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        // 自定义属性名 都要默认值的
        treeNodeConfig.setWeightKey("sortNumber");
        // 最大递归深度
        treeNodeConfig.setDeep(10);
        //转换器
        return cn.hutool.core.lang.tree.TreeUtil.build(menus, "0", treeNodeConfig,
                (treeNode, tree) -> {
                    tree.setId(treeNode.getCode());
                    tree.setParentId(treeNode.getParentCode());
                    tree.setWeight(treeNode.getSort());
                    tree.setName(treeNode.getName());
                    // 扩展属性 ...
                    tree.putExtra("type", treeNode.getType());
                    tree.putExtra("code", treeNode.getCode());
                    tree.putExtra("sourceId", treeNode.getId()+"");
                    tree.putExtra("router", treeNode.getRouter());
                    tree.putExtra("icon", treeNode.getIcon());
                    tree.putExtra("status", treeNode.getStatus());
                    tree.putExtra("component", treeNode.getComponent());
                    tree.putExtra("componentName", treeNode.getComponentName());
                });
    }
}
