package ${package}.controller.sys.dto;

import cn.tannn.jdevelops.annotations.jpa.JpaSelectIgnoreField;
import cn.tannn.jdevelops.annotations.jpa.JpaSelectOperator;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLConnect;
import cn.tannn.jdevelops.annotations.jpa.enums.SQLOperatorWrapper;
import cn.tannn.jdevelops.jpa.request.PagingSorteds;
import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 字典表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
@Schema(description = "分页查询字典表")
@ToString
@Getter
@Setter
public class DictionaryPage extends SerializableBean<DictionaryPage> {


    /** 代码;同级唯一 */
    @Schema(description = "代码(同级唯一)")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
    private  String  code ;


    /** 名称;同级唯一 */
    @Schema(description = "名称(同级唯一)")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
    private  String  name ;


    /** 父级代码 */
    @Schema(description = "父级代码")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.LIKE, connect = SQLConnect.AND)
    private  String  parentCode ;


    /** 状态;0、不可用，1、可用，默认1 */
    @Schema(description = "状态(0、不可用，1、可用，默认1)")
    @JpaSelectOperator(operatorWrapper = SQLOperatorWrapper.EQ, connect = SQLConnect.AND)
    private  Integer  status ;


    /**
     * 分页排序
     */
    @Schema(description = "分页排序")
    @JpaSelectIgnoreField
    @Valid
    private PagingSorteds page;

    public PagingSorteds getPage() {
        if(page == null){
            return new PagingSorteds().fixSort(0,"sort");
        }
        return page.append(0,"sort");
    }
}
