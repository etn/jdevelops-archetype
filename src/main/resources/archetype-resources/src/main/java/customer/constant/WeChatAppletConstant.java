package ${package}.customer.constant;

/**
 * 微信常量
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-12-29 16:33
 */
public interface WeChatAppletConstant {

    /**
     * 微信的 wecaht_session_key 存reids的key
     */
    String REDIS_WECAHT_SESSION_KEY= "wecaht_session_key";

    /**
     * 微信code有效时间 默认600
     */
    Integer AUTHORIZATION_CODE_TIME = 600;
}
