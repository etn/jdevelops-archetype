package ${package}.controller.user.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.constraints.NotBlank;

/**
 * @author tan
 */
@Schema(description = "重置密码")
@ToString
@Getter
@Setter
public class PasswordReset {
    /**
     * 需要重置密码的账户
     */
    @Schema(description = "需要重置密码的账户", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String loginName;

}
