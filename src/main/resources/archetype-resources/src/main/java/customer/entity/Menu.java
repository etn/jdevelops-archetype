package ${package}.customer.entity;


import ${package}.customer.constant.MenuStatus;
import ${package}.customer.constant.MenuType;
import ${package}.common.pojo.JpaCommonBean;
import ${package}.controller.user.dto.MenuAlter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;

/**
 * 菜单路由
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-7
 */
@Entity
@Table(name = "tb_menu", indexes = {
        @Index(name = "code_index", columnList = "code", unique = true),
})
@Comment("菜单路由")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
@DynamicInsert
public class Menu extends JpaCommonBean<Menu> {



    /**
     * 编号
     */
    @Comment("编号")
    @Column(columnDefinition = "varchar(50)  not null")
    private String code;


    /**
     * 名称;同级名称唯一
     */
    @Comment("名称;同级名称唯一")
    @Column(columnDefinition = "varchar(100)  not null")
    private String name;


    /**
     * 父级序号;唯一
     */
    @Comment("父级序号;唯一")
    @Column(columnDefinition = "varchar(50)  not null")
    private String parentCode;

    /**
     * 父级名称
     */
    @Comment("父级名称")
    @Column(columnDefinition = "varchar(100)  not null")
    private String parentName;


    /**
     * 排序;数字越小排在越前面
     */
    @Comment("排序;数字越小排在越前面")
    @Column(columnDefinition = "int")
    private Integer sort;


    /**
     * 前端组件;父级序号
     */
    @Comment("前端组件;父级序号")
    @Column(columnDefinition = "varchar(255) ")
    private String component;


    /**
     * 前端组件名称
     */
    @Comment("前端组件名称")
    @Column(columnDefinition = "varchar(255) ")
    private String componentName;


    /**
     * 图标
     */
    @Comment("图标")
    @Column(columnDefinition = "varchar(255)")
    private String icon;


    /**
     * 节点路由
     */
    @Comment("节点路由")
    @Column(columnDefinition = "varchar(255)")
    private String router;


    /**
     * 类型;0、节点；1、功能；2、按钮
     *
     * @see MenuType
     */
    @Comment("类型;0、节点；1、功能；2、按钮")
    @Column(columnDefinition = "int default 0")
    private Integer type;


    /**
     * 节点状态;0、禁用；1、启用；默认启用
     *
     * @see MenuStatus
     */
    @Comment("节点状态;0、禁用；1、启用；默认启用")
    @Column(columnDefinition = "int default 1")
    private Integer status;


    public static Menu top() {
        Menu top = new Menu();
        top.setCode("0");
        top.setName("顶级菜单");
        return top;
    }


    public Menu alterMenuToMenu(MenuAlter alter) {

        if (StringUtils.isNotBlank(alter.getCode())) {
            this.code = alter.getCode();
        }

        if (StringUtils.isNotBlank(alter.getName())) {
            this.name = alter.getName();
        }

        if (alter.getType() != null) {
            this.type = alter.getType();
        }

        if (StringUtils.isNotBlank(alter.getRouter())) {
            this.router = alter.getRouter();
        }

        if (StringUtils.isNotBlank(alter.getParentCode())) {
            this.parentCode = alter.getParentCode();
        }

        if (alter.getSort() != null) {
            this.sort = alter.getSort();
        }

        if (alter.getIcon() != null) {
            this.icon = alter.getIcon();
        }

        if (alter.getStatus() != null) {
            this.status = alter.getStatus();
        }

        if (StringUtils.isNotBlank(alter.getComponent())) {
            this.component = alter.getComponent();
        }

        if (StringUtils.isNotBlank(alter.getComponentName())) {
            this.componentName = alter.getComponentName();
        }
        return this;
    }



}
