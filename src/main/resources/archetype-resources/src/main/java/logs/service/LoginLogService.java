package ${package}.logs.service;

import ${package}.controller.logs.dto.LoginLogRecord;
import ${package}.logs.entity.LoginLog;
import cn.tannn.jdevelops.jpa.service.J2Service;

/**
 * 登录日志
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2024/4/8 10:54
 */
public interface LoginLogService  extends J2Service<LoginLog> {

    /**
     * 登录日志记录
     * @param recordLog LoginLogRecord
     */
    void recordLog(LoginLogRecord recordLog);
}
