package ${package}.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/10/30 上午11:17
 */
public class CodeUtil {

    /**
     * 获取字段code的层级
     * @return 层级 [1~n]
     */
    public static int dictCodeLevel(String code) {
        if(code == null){
            throw new IllegalArgumentException("非法参数");
        }
        int level = code.length() / 3;
        if(level == 0){
            throw new IllegalArgumentException("code.length() 必须能被 3 整除");
        }
        return level;
    }

}
