package ${package}.controller.user.dto;

import ${package}.customer.constant.CustomerStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

/**
 * 设置用户状态（ 0、停用，1、正常，2、封禁）
 *
 * @author tnnn
 * @version V1.0
 * @date 2022-11-09 13:33
 */
@Schema(description = "设置用户状态")
@ToString
@Getter
@Setter
public class CustomerSettingStatus {

    /**
     * 用户id
     */
    @Schema(description = "用户id",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    List<Long> id;

    /**
     * 状态:1[正常],2[锁定],3[逻辑删除]
     */
    @Schema(description = "状态:1[正常],2[锁定],3[逻辑删除]",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull
    Integer status;


    public CustomerSettingStatus() {
    }

    /**
     *
     * @param id 用户id
     * @param status 状态:1[正常],2[锁定],3[逻辑删除]
     */
    public CustomerSettingStatus(List<Long> id, Integer status) {
        this.id = id;
        this.status = status;
    }


    /**
     *
     * @param id 用户id
     * @param status UserStatus
     */
    public CustomerSettingStatus(Long id, CustomerStatus status) {
        this.id = Collections.singletonList(id);
        this.status = status.getStatus();
    }


}
