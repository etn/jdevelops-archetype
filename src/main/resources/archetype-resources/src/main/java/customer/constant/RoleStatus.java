package ${package}.customer.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 角色状态
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/8/28 13:21
 */
@AllArgsConstructor
@Getter
public enum RoleStatus {

    /**
     * 停用
     */
    DISABLE(0, "停用"),


    /**
     * 正常
     */
    NORMAL(1, "启用"),

    ;

    private final int status;
    private final String description;


    /**
     * 获取枚举实例
     * @param type int
     * @return RoleType
     */
    public static RoleStatus fromValue(Integer type) {
        for (RoleStatus roleStatus : RoleStatus.values()) {
            if (Objects.equals(type, roleStatus.getStatus())) {
                return roleStatus;
            }
        }
        throw new IllegalArgumentException();
    }

}
