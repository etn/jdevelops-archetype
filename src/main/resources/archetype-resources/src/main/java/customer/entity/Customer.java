package ${package}.customer.entity;

import ${package}.common.pojo.JpaCommonBean;
import cn.hutool.crypto.SecureUtil;
import cn.tannn.jdevelops.exception.built.UserException;
import cn.tannn.jdevelops.jwt.standalone.exception.DisabledAccountException;
import cn.tannn.jdevelops.utils.jwt.constant.UserStatusMark;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.time.LocalDateTime;

import static cn.tannn.jdevelops.utils.jwt.exception.UserCode.USER_PASSWORD_ERROR;

/**
 * 用户
 *
 * @author tnnn
 * @version V1.0
 * @date 2023-10-25
 */
@Entity
@Table(name = "tb_customer")
@Comment("用户")
@Getter
@Setter
@ToString
@DynamicUpdate
@DynamicInsert
public class Customer extends JpaCommonBean<Customer> {
    /**
     * 登录名
     */
    @Column(columnDefinition = " varchar(100)  not null ")
    @Comment("登录名")
    private String loginName;

    /**
     * 登录密码
     */
    @Column(columnDefinition = " varchar(100) not null ")
    @Comment("登录密码")
    private String password;


    /**
     * 用户头像
     */
    @Column(columnDefinition = " varchar(255)")
    @Comment("用户头像")
    private String icon;

    /**
     * 用户姓名
     */
    @Column(columnDefinition = " varchar(255)")
    @Comment("用户姓名")
    private String name;

    /**
     * 用户昵称
     */
    @Column(columnDefinition = " varchar(200)")
    @Comment("用户昵称")
    private String nickname;


    /**
     * 性别:0[未知]，1[男性]，2[女性]
     */
    @Column(columnDefinition = " int   default 0")
    @Comment("性别:0[未知]，1[男性]，2[女性]")
    private Integer gender;



    /**
     * 状态:1[正常],2[锁定],3[逻辑删除]
     */
    @Column(columnDefinition = " int default 1")
    @Comment("状态:1[正常],2[锁定],3[逻辑删除]")
    private Integer status;

    /**
     * 状态标记说明
     *
     * @see UserStatusMark
     */
    @Column(columnDefinition = " varchar(200)")
    @Comment("状态标记说明")
    String statusMark;


    /**
     * 审核状态:1[审核中],2[审核通过],3[审核不通过]
     */
    @Column(columnDefinition = " int default 1")
    @Comment("审核状态:1[审核中],2[审核通过],3[审核不通过]")
    private Integer auditStatus;


    /**
     * 审核时间
     */
    @Column(columnDefinition = " timestamp")
    @Comment("审核时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;

    /**
     * 审核备注
     */
    @Column(columnDefinition = " varchar(200)")
    @Comment("审核备注")
    private String auditRemark;

    /**
     * 备注
     */
    @Column(columnDefinition = " varchar(200)")
    @Comment("备注")
    private String remark;


    /**
     * 用户输入跟数据库密码对比
     * @param loginName 用户输入的登录名(不使用内置的处理数据库忽略大小写的问题)
     * @param inputPass 用户输入密码
     */
    public void verifyUserPassMd5(String loginName, String inputPass) {
        String loginPwdLocal = getMd5Password(loginName, inputPass.trim());
        if (!StringUtils.equals(loginPwdLocal, password)) {
            throw new UserException(USER_PASSWORD_ERROR);
        }
    }

    /**
     * 获取md5密码（用户密码）
     *
     * @param loginName       用户名
     * @param settingPassword 需要设置的密码
     * @return String
     */
    public static String getMd5Password(String loginName, String settingPassword) {
        return SecureUtil.md5(settingPassword.trim() + loginName.trim());
    }

    /**
     *  验证用户状态
     */
    public void verifyStatus() {
        if (status != null && status != 1) {
            throw new DisabledAccountException(400, statusMark);
        }
    }

    public void fixNickname(String nickname) {
        if (StringUtils.isBlank(this.nickname)) {
            this.setNickname(nickname);
        }
    }

    public void fixIcon(String icon) {
        if (StringUtils.isBlank(this.icon)) {
            this.setIcon(icon);
        }
    }

    public void fixGender(Integer gender) {
        if (this.gender == null) {
            this.setGender(gender);
        }
    }
}
