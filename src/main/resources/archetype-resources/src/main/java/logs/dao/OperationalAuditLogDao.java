package ${package}.logs.dao;

import ${package}.jpa.repository.JpaBasicsRepository;
import ${package}.logs.entity.OperationalAuditLog;

/**
 * 数据审计日志
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/23 09:46
 */
public interface OperationalAuditLogDao extends JpaBasicsRepository<OperationalAuditLog, Long> {
}
