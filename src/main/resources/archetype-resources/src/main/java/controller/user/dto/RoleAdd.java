package ${package}.controller.user.dto;

import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.constraints.NotBlank;

/**
 * 角色管理-新增
 *
 * @author tn
 * @date 2021-06-10 15:21
 */
@Schema(description = "新增角色")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleAdd extends SerializableBean<RoleAdd> {

    /**
     * 名称，唯一
     */
    @Schema(description = "角色名", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String name;
    /**
     * 代号，唯一
     */
    @Schema(description = "角色编码", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank
    private String code;

    /**
     * 状态;0[停用] ，1[启用]
     */
    @Schema(description = "状态;0[停用] ，1[启用]")
    private Integer status;

    /**
     * 排序;数字越小排在最前面
     */
    @Schema(description = "排序;数字越小排在最前面")
    private Integer sort;

    /**
     * 角色描述
     */
    @Schema(description = "角色描述")
    private String description;


    public void setStatus(Integer status) {
        if (status == null) {
            this.status = 1;
        }else {
            this.status = status;
        }
    }

    public void setSort(Integer sort) {
        if (sort == null) {
            this.sort = 1;
        }else {
            this.sort = sort;
        }
    }
}
