package ${package}.controller.user;

import ${package}.customer.constant.CustomerStatus;
import ${package}.customer.constant.UserLoginNameDef;
import ${package}.controller.user.dto.CustomerRegisterAttach;
import ${package}.customer.service.CustomerService;
import ${package}.controller.user.dto.CustomerRegister;
import cn.tannn.jdevelops.annotations.web.authentication.ApiMapping;
import cn.tannn.jdevelops.annotations.web.authentication.ApiPlatform;
import cn.tannn.jdevelops.annotations.web.constant.PlatformConstant;
import cn.tannn.jdevelops.annotations.web.mapping.PathRestController;
import cn.tannn.jdevelops.result.response.ResultVO;
import cn.tannn.jdevelops.utils.jwt.constant.UserStatusMark;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.extensions.Extension;
import io.swagger.v3.oas.annotations.extensions.ExtensionProperty;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.validation.Valid;

/**
 * 注册管理
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/10/26 11:51
 */

@Tag(name = "注册管理", description = "注册管理",
        extensions = {
                @Extension(properties = {@ExtensionProperty(name = "x-order", value = "2", parseValue = true)})}
)
@PathRestController("register")
@RequiredArgsConstructor
@Slf4j
@Validated
public class RegisterController {

    private final CustomerService customerService;

    @Operation(summary = "管理员主动新增")
    @ApiMapping(value = "system", method = RequestMethod.POST)
    @ApiPlatform(platform = PlatformConstant.WEB_ADMIN)
    public ResultVO<String> admin(@RequestBody @Valid CustomerRegister register) {
        if (UserLoginNameDef.SUPPER_USER.contains(register.getLoginName().toLowerCase())) {
            return ResultVO.failMessage("非法注册用户");
        }
        if (customerService.registerUser(register
                , new CustomerRegisterAttach(2,CustomerStatus.NORMAL, "正常账户", "管理员主动新增用户"))) {
            if (log.isDebugEnabled()) {
                log.debug("account: {}, sign up success", register);
            }
            // 注册成功刷新用户资源权限缓存
            return ResultVO.successMessage("账户注册成功");
        } else {
            return ResultVO.failMessage("用户已经存在,请修改用户名后在注册");
        }
    }


    @Operation(summary = "用户注册")
    @ApiMapping(value = "myself", method = RequestMethod.POST, checkToken = false)
    @ApiPlatform(platform = PlatformConstant.WEB_ADMIN)
    public ResultVO<String> myself(@RequestBody @Valid CustomerRegister register) {
        if (UserLoginNameDef.SUPPER_USER.contains(register.getLoginName().toLowerCase())) {
            return ResultVO.failMessage("非法注册用户");
        }
        if (customerService.registerUser(register
                , new CustomerRegisterAttach(1, CustomerStatus.BANNED, UserStatusMark.WAIT_AUDIT, "用户注册"))) {
            if (log.isDebugEnabled()) {
                log.debug("account: {}, sign up success", register);
            }
            // 注册成功刷新用户资源权限缓存
            return ResultVO.successMessage("账户注册成功");
        } else {
            return ResultVO.failMessage("用户已经存在,请修改用户名后在注册");
        }
    }
}
