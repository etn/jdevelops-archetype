package ${package}.customer.service;

import ${package}.customer.entity.RoleCustomer;
import cn.tannn.jdevelops.jpa.service.J2Service;

import java.util.Collection;
import java.util.List;

/**
 * 角色 - 系统用户
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:11
 */
public interface RoleCustomerService extends J2Service<RoleCustomer> {


    /**
     * 根据角色code查询用户集[用户id]
     * @param roleCode 角色编号
     * @return userid
     */
    List<Long> getUserIdByRoleCode(String roleCode);

    /**
     * 根据角色code查询用户集[用户id]
     * @param roleCode 角色编号
     * @return userid
     */
    List<Long> getUserIdByRoleCode(List<String> roleCode);

    /**
     * 根据角色code查询用户集[用户id]
     * @param roleCode 角色编号
     * @return 登录名
     */
    List<String> getUserLoginNameByRoleCode(List<String> roleCode);

    /**
     * 删除角色-用户
     * @param roleCode 角色
     * @param userIds 用户
     * @return boolean
     */
    boolean deleteByRoleCodeAndUserIds(String roleCode, Collection<Long> userIds);

    /**
     * 删除角色-用户
     * @param roleCodes 角色
     * @return 返回别删除的 userLoginName
     */
    List<String>  deleteByRoleCode(List<String> roleCodes);


    /**
     * 查询角色编号集合
     * @param userId 用户序号
     * @return RoleCode
     */
    List<String> getRoleCodeByUserId(Long userId);

}
