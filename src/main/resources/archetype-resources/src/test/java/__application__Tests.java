package  ${package};


import ${package}.customer.constant.RoleDef;
import ${package}.customer.constant.RoleStatus;
import ${package}.customer.constant.RoleType;
import ${package}.customer.constant.UserLoginNameDef;
import ${package}.customer.entity.Customer;
import ${package}.customer.entity.Role;
import ${package}.customer.entity.RoleCustomer;
import ${package}.customer.service.CustomerService;
import ${package}.customer.service.RoleCustomerService;
import ${package}.customer.service.RoleService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

@SpringBootTest
class ${application}Tests {


    @Test
    void contextLoads() {
    }

    @Autowired
    private CustomerService customerService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleCustomerService roleCustomerService;


    //    @Test
    void initRoleInfo() {
        Role sysRole = new Role();
        sysRole.setCode(RoleDef.USER_ADMIN);
        sysRole.setName("系统管理员");
        sysRole.setSort(1);
        sysRole.setType(RoleType.DEF.getType());
        sysRole.setStatus(RoleStatus.NORMAL.getStatus());
        roleService.saveOne(sysRole);

        Role sysRole2 = new Role();
        sysRole2.setCode(RoleDef.USER_COMMON);
        sysRole2.setName("普通用户");
        sysRole2.setSort(2);
        sysRole2.setType(RoleType.DEF.getType());
        sysRole2.setStatus(RoleStatus.NORMAL.getStatus());
        roleService.saveOne(sysRole2);

    }

    //    @Test
    void initUserInfo() {
        Customer entity = new Customer();
        entity.setLoginName(UserLoginNameDef.ADMIN);
        entity.setPassword(Customer.getMd5Password(UserLoginNameDef.ADMIN, "123"));
        entity.setIcon("");
        entity.setName("系统管理员");
        entity.setNickname("系统管理员");
        entity.setGender(0);
        entity.setStatus(1);
        entity.setStatusMark("正常用户");
        entity.setAuditStatus(2);
        entity.setAuditRemark("系统注册");
        entity.setAuditTime(LocalDateTime.now());
        entity.setCreateTime(LocalDateTime.now());
        entity.setCreateUserName(UserLoginNameDef.ADMIN);
        entity.setUpdateTime(LocalDateTime.now());
        entity.setUpdateUserName(UserLoginNameDef.ADMIN);

        customerService.saveOne(entity);

        try {
            RoleCustomer roleCustomer = new RoleCustomer();
            roleCustomer.setUserId(entity.getId());
            roleCustomer.setRoleCode(RoleDef.USER_ADMIN);
            roleCustomerService.saveOne(roleCustomer);
        }catch (Exception e){

        }

    }

}

