package ${package}.customer.service.impl;


import ${package}.customer.dao.RoleMenuDao;
import ${package}.customer.entity.RoleMenu;
import ${package}.customer.service.RoleMenuService;
import cn.tannn.jdevelops.jpa.service.J2ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 角色-菜单
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:40
 */
@Service
@Slf4j
public class RoleMenuServiceImpl extends J2ServiceImpl<RoleMenuDao,RoleMenu,Long> implements RoleMenuService {
    public RoleMenuServiceImpl() {
        super(RoleMenu.class);
    }

    @Override
    public List<String> getMenuCodesByRoleCode(String roleCode) {
        return getJpaBasicsDao().findMenuCodeByRoleCode(roleCode);
    }

    @Override
    public List<String> getMenuCodesByRoleCode(List<String> roleCodes) {
        return getJpaBasicsDao().findMenuCodeByRoleCode(roleCodes);
    }

    @Override
    public boolean deleteByRoleCode(String roleCode) {
        try {
            getJpaBasicsDao().removeByRoleCode(roleCode);
        } catch (Exception e) {
            log.error("删除角色-菜单失败", e);
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteByRoleCode(List<String> roleCode) {
        try {
            getJpaBasicsDao().removeByRoleCodeIn(roleCode);
        } catch (Exception e) {
            log.error("删除角色-菜单失败", e);
            return false;
        }
        return true;
    }
}
