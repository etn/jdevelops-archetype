package ${package}.sys.dao;

import cn.tannn.jdevelops.jpa.repository.JpaBasicsRepository;
import ${package}.sys.entity.SysConfig;

/**
 * 系统配置表
 *
 * @author tnnn
 * @version V1.0
 * @date 2024-10-9
 */
public interface SysConfigDao extends JpaBasicsRepository<SysConfig,Integer> {

}
