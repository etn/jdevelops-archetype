package ${package}.controller.user.dto;


import cn.tannn.jdevelops.result.bean.SerializableBean;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色权限-功能节点
 *
 * @author tn
 * @date 2021-06-10 16:03
 */
@Schema(description = "角色设置菜单")
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleMenus extends SerializableBean<RoleMenus> {
	/**
	 * 角色编号
	 */
	@Schema(description = "角色编码", requiredMode = Schema.RequiredMode.REQUIRED)
	@NotNull
	private String roleCode;

	/**
	 * 菜单编号集合
	 */
	@Schema(description = "菜单编号集合", requiredMode = Schema.RequiredMode.REQUIRED)
	private List<String> menuCodes;
}
