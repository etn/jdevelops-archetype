package ${package}.customer.service;

import cn.hutool.core.lang.tree.Tree;
import ${package}.customer.constant.MenuStatus;
import ${package}.customer.constant.MenuType;
import ${package}.customer.entity.Menu;
import cn.tannn.jdevelops.jpa.service.J2Service;


import java.util.List;
import java.util.Optional;

/**
 * 菜单
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:08
 */
public interface MenuService extends J2Service<Menu> {
    /**
     * 禁用节点
     * 禁用自己及其子级
     *
     * @param codes codes
     */
    void disable(List<String> codes);

    /**
     * 启用节点
     * 启用父节点
     *
     * @param codes codes
     */
    void enabled(List<String> codes);


    /**
     * 获取所有节点，但不包括禁用的节点
     *
     * @return Menu
     */
    List<Menu> getAllExDisabled();


    /**
     * 组装功能节点树
     *
     * @param beanList Menu
     * @return Tree
     */
    List<Tree<String>> getTree(List<Menu> beanList);

    /**
     * 验证父节点是否有效 (返回值用来修改父级名)
     *
     * @param parentCode parentCode
     * @return Menu
     */
    Optional<Menu> verifyParentExist(String parentCode);

    /**
     * 验证统计名称是否重复
     *
     * @param name       name
     * @param parentCode parentCode
     * @param code       编号
     */
    void verifyName(String name, String parentCode, String code);

    /**
     * 查询路由
     *
     * @param code code
     * @return Menu
     */
    Optional<Menu> findByCode(String code);


    /**
     * 查询路由
     *
     * @param codes codes
     * @return Menu of List
     */
    List<Menu> findByCode(List<String> codes);


    /**
     * 删除自己包括自己的子级
     *
     * @param codes codes
     * @author tan
     * @date 2021/7/13 11:33
     */
    void deleteMenu(List<String> codes);


    /**
     * 获取下一级的节点
     *
     * @param code  code
     * @param status 节点状态（0、禁用；1、启用） 不传默认查所有
     * @param type  类型（0、节点；1、功能；2、按钮） 不传默认查所有
     * @return Menu
     */
    List<Menu> getChild(final String code, final MenuStatus status, final MenuType type);


    /**
     * 根据id查询
     *
     * @param status 节点状态（0、禁用；1、启用）默认启用
     * @param codes code集合
     * @return Menu of List
     */
    List<Menu> findByMenuStatusAndCodeIn(Integer status, List<String> codes);
}
