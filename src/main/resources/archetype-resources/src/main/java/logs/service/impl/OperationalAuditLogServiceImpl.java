package ${package}.logs.service.impl;

import ${package}.jpa.service.J2ServiceImpl;
import ${package}.logs.dao.OperationalAuditLogDao;
import ${package}.logs.entity.OperationalAuditLog;
import ${package}.logs.service.OperationalAuditLogService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.scheduling.annotation.Async;

/**
 * 数据审计日志
 *
 * @author <a href="https://t.tannn.cn/">tan</a>
 * @version V1.0
 * @date 2024/12/23 09:49
 */
@Slf4j
@Service
public class OperationalAuditLogServiceImpl extends J2ServiceImpl<OperationalAuditLogDao, OperationalAuditLog, Long>
        implements OperationalAuditLogService {

    public OperationalAuditLogServiceImpl() {
        super(OperationalAuditLog.class);
    }


}
