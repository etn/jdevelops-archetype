package ${package}.customer.service;


import ${package}.customer.entity.RoleMenu;
import cn.tannn.jdevelops.jpa.service.J2Service;

import java.util.List;

/**
 * 角色-菜单
 *
 * @author <a href="https://tannn.cn/">tan</a>
 * @date 2023/12/4 13:11
 */
public interface RoleMenuService extends J2Service<RoleMenu> {
    /**
     * 查询功能节点序号集合
     *
     * @param roleCode 角色code
     * @return code of List
     */
    List<String> getMenuCodesByRoleCode(String roleCode);

    /**
     * 查询功能节点序号集合
     *
     * @param roleCodes 角色code
     * @return code of List
     */
    List<String> getMenuCodesByRoleCode(List<String> roleCodes);

    /**
     * 删除 角色-菜单
     * @param roleCode 角色code
     * @return boolean
     */
    boolean deleteByRoleCode(String roleCode);


    /**
     * 删除 角色-菜单
     * @param roleCode 角色code
     * @return boolean
     */
    boolean deleteByRoleCode(List<String> roleCode);
}
